<!doctype html>
<!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7" lang=""> <![endif]-->
<!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8" lang=""> <![endif]-->
<!--[if IE 8]>         <html class="no-js lt-ie9" lang=""> <![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js" lang=""> <!--<![endif]-->
    <head>
        <!-- Global site tag (gtag.js) - Google Analytics -->
        <script async src="https://www.googletagmanager.com/gtag/js?id=UA-162812255-1"></script>
        <script>
          window.dataLayer = window.dataLayer || [];
          function gtag(){dataLayer.push(arguments);}
          gtag('js', new Date());

          gtag('config', 'UA-162812255-1');
        </script>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
        <title>{{ Voyager::setting('site.title') }} | @yield('title')</title>
        @stack('meta')
        <meta name="description" content="{{ Voyager::setting('site.description') }}">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta name="csrf-token" content="{{ csrf_token() }}" />
        <link rel="icon" href="{{ asset('assets/images/ala_favicon.ico') }}" type="image/x-icon">

        <!--Google Fonts link-->
        <link href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,300i,400,400i,600,600i,700,700i" rel="stylesheet">
        <link href="https://fonts.googleapis.com/css?family=Open+Sans:300,400,400i,600,600i,700,700i" rel="stylesheet">


        <link rel="stylesheet" href="{{ asset('assets/css/iconfont.css') }}">
        <link rel="stylesheet" href="{{ asset('assets/css/slick/slick.css') }}">
        <link rel="stylesheet" href="{{ asset('assets/css/slick/slick-theme.css') }}">
        <link rel="stylesheet" href="{{ asset('assets/css/font-awesome.min.css') }}">
        <link rel="stylesheet" href="{{ asset('assets/css/jquery.fancybox.css') }}">
        <link rel="stylesheet" href="{{ asset('assets/css/bootstrap.css') }}">
        <link rel="stylesheet" href="{{ asset('assets/css/bootstrap.min.css') }}">
        <link rel="stylesheet" href="{{ asset('assets/css/magnific-popup.css') }}">


        <!--For Plugins external css-->
        <link rel="stylesheet" href="{{ asset('assets/css/plugins.css') }}" />

        <!--Theme custom css -->
        <link rel="stylesheet" href="{{ asset('assets/css/style.css') }}">

        <!--Theme Responsive css-->
        <link rel="stylesheet" href="{{ asset('assets/css/responsive.css') }}" />

        <script src="{{ asset('assets/js/vendor/modernizr-2.8.3-respond-1.4.2.min.js') }}"></script>

        <style type="text/css">
        .modal-dialog{
            z-index: 99999999;
        }
        </style>

    </head>
    <body data-spy="scroll" data-target=".navbar-collapse">

        <div class='preloader'><div class='loaded'>&nbsp;</div></div>
        <div class="culmn">
            @include('client.components.header')

            @yield('content')

            @include('client.components.footer')

            <!-- Modal -->
            <div class="modal fade" id="privacymodal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLongTitle" aria-hidden="true">
                <div class="modal-dialog modal-lg modal-dialog-centered" role="document">
                    <div class="modal-content">
                        <div class="modal-header">
                            <h4 class="modal-title" id="exampleModalLongTitle">Kebijakan Privasi</h4>
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button>
                        </div>
                        <div class="modal-body">
                            <h4>A. UMUM</h4>
                            <p style="text-align: justify;">
                                Selamat datang di website Alametric maupun aplikasi Alametric (“Situs”), halaman ini dikelola oleh tim Alametric (“Kami”), kebijakan privasi yang dimaksud pada halaman ini (“Kebijakan”) adalah acuan untuk mengatur dan melindungi penggunaan data dan informasi penting para pengguna Situs (“Anda”). Sebelum Anda menggunakan atau memberikan informasi apapun kepada Situs ini, mohon untuk membaca dan memahami terlebih dahulu mengenai Kebijakan privasi yang telah Kami buat ini. Dengan menggunakan Situs ini maka pengguna dianggap telah membaca, mengerti, memahami, dan menyetujui semua isi dalam Kebijakan ini. 
                            </p>
                            <p style="text-align: justify;">
                                Kami berhak atas kebijakan untuk mengubah atau memodifikasi sebagian atau keseluruhan dari isi Kebijakan ini setiap saat, artinya aturan yang berlaku pada halaman ini dapat berubah sewaktu-waktu secara sepihak oleh Kami, apabila hal tersebut terjadi maka Kami akan mencoba memberikan pemberitahuan kepada seluruh pengguna Situs, bisa melalui email, sosial media Kami, maupun melalui Situs ini secara langsung. Aturan yang baru akan mulai berlaku setelah pemberitahuan sudah terpublikasikan atau terkirim ke seluruh pengguna Situs. Kesepakatan di atas tidak berlaku apabila terdapat perubahan karena alasan hukum Negara Indonesia, Kebijakan pada Situs ini akan berubah menyesuaikan dengan peraturan pemerintah yang berlaku.
                            </p>
                            <h4>B. PERSETUJUAN</h4>
                            <p style="text-align: justify;">
                                Informasi terlebih lanjut akan Kami jelaskan di bawah ini, bila Anda menggunakan atau sebaliknya mengakses layanan Situs, Kami mungkin mengumpulkan informasi Anda dalam berbagai cara, termasuk ketika Anda memberikan informasi secara langsung kepada Kami, dan ketika Kami secara pasif mengumpulkan informasi dari Situs.
                            </p>
                            <h4>C. PENGGUNAAN INFORMASI</h4>
                            <p style="text-align: justify;">
                                Kami menggunakan informasi pribadi Anda untuk berbagai keperluan verifikasi, seperti verifikasi akun, rekening atau identitas lainnya. Hal tersebut diperlukan agar terdapat kejelasan informasi dan tujuan dalam menggunakan Situs Kami.
                                Kami menggunakan alamat email yang Anda berikan untuk mengirimkan pembaharuan atau pesan dari Situs Kami baik secara berkala maupun saat itu juga.
                                Dalam hal tertentu Kami menggunakan informasi Anda untuk meningkatkan mutu dan pelayanan Situs.
                                Kami tidak menjual, menyewakan dan memberikan informasi Anda kepada pihak lain, kecuali apabila ditentukan pada persyaratan lain dan diminta oleh pihak yang berwajib menghendaki untuk meminta informasi kepada Kami.
                            </p>
                            <h4>D. KEAMANAN DATA</h4>
                            <p style="text-align: justify;">
                                Kami tidak pernah memberikan informasi pribadi Anda kepada pihak ketiga, kecuali apabila ketentuan hukum berkehendak lain. Kami menjaga keamanan dan kerahasiaan informasi Anda yang telah masuk dalam database Kami. Untuk mencegah akses yang tidak sah, menjaga keakuratan Data dan memastikan penggunaan informasi yang benar, apabila terdapat informasi yang menurut Anda yang bersifat privasi atau rahasia akan Kami simpan sesuai dengan ketentuan yang disepakati.
                            </p>
                            <h4>E. PEMBERITAHUAN</h4>
                            <p style="text-align: justify;">
                                Pengunjung Platform atau Pengguna Platform sepakat dan bersedia untuk menerima segala notifikasi melalui media elektronik, termasuk tidak terbatas pada email, layanan pesan singkat (short messaging service atau SMS) dan/atau pesan instan (instant messaging) yang didaftarkannya pada Platform dan untuk itu memberikan izin kepada Penyedia Platform untuk menghubungi Pengunjung Platform atau Pengguna Platform melalui media elektronik tersebut.
                            </p>
                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-primary" data-dismiss="modal">Close</button>
                        </div>
                    </div>
                </div>
            </div>

        </div>

        <!-- START SCROLL TO TOP  -->

        <div class="scrollup">
            <a href="#"><i class="fa fa-chevron-up"></i></a>
        </div>

        <script src="{{ asset('assets/js/vendor/jquery-1.11.2.min.js') }}"></script>
        <script src="{{ asset('assets/js/vendor/bootstrap.min.js') }}"></script>

        <script src="{{ asset('assets/js/jquery.magnific-popup.js') }}"></script>
        <script src="{{ asset('assets/js/jquery.mixitup.min.js') }}"></script>
        <script src="{{ asset('assets/js/jquery.easing.1.3.js') }}"></script>
        <script src="{{ asset('assets/js/jquery.masonry.min.js') }}"></script>

        <!--slick slide js -->
        <script src="{{ asset('assets/css/slick/slick.js') }}"></script>
        <script src="{{ asset('assets/css/slick/slick.min.js') }}"></script>


        <script src="{{ asset('assets/js/plugins.js') }}"></script>
        <script src="{{ asset('assets/js/main.js') }}"></script>

        <!-- Getbutton.io widget -->
        <script type="text/javascript">
            (function () {
                var options = {
                    whatsapp: "+6282123601315", // WhatsApp number
                    call_to_action: "Halo kami Alametric, bisa kami bantu?", // Call to action
                    position: "right", // Position may be 'right' or 'left'
                };
                var proto = document.location.protocol, host = "getbutton.io", url = proto + "//static." + host;
                var s = document.createElement('script'); s.type = 'text/javascript'; s.async = true; s.src = url + '/widget-send-button/js/init.js';
                s.onload = function () { WhWidgetSendButton.init(host, proto, options); };
                var x = document.getElementsByTagName('script')[0]; x.parentNode.insertBefore(s, x);
            })();
        </script>
        <!-- /Getbutton.io widget -->

    </body>
</html>
