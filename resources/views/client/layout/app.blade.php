<!doctype html>
<html class="no-js" lang="en">

<head>
    <!-- Global site tag (gtag.js) - Google Analytics -->
    <script async src="https://www.googletagmanager.com/gtag/js?id=UA-162812255-1"></script>
        <script>
          window.dataLayer = window.dataLayer || [];
          function gtag(){dataLayer.push(arguments);}
          gtag('js', new Date());

          gtag('config', 'UA-162812255-1');
          gtag('config', 'AW-599431799': { "groups": "default" });

        </script>
    <!-- Google Tag -->
    <meta name="google-site-verification" content="iJAmliceAQaTDwX-JjeFirtAAYt9Z6DuOBSW-Q8Tnso" />
    <meta name="google-signin-client_id" content="{{ config('app.google_id', '') }}.apps.googleusercontent.com">
    <meta charset="utf-8">
    <meta http-equiv="x-ua-compatible" content="ie=edge">
    <title>{{ Voyager::setting('site.title') }} | @yield('title')</title>
    @stack('meta')
    <meta name="description" content="Alametric.com adalah platform sosial media terintegrasi yang melayani dan menangani aktivitas media sosial anda keseluruhan baik di Facebook, Instagram, Twitter, YouTube, dan lain sebagainya.">
	<meta name="robots" content="index, follow">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="csrf-token" content="{{ csrf_token() }}" />
    <!-- Favicon -->
    <link rel="icon" href="{{ asset('assets/images/favicon.ico') }}" type="image/x-icon">

    <!-- CSS
	============================================ -->
    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="{{ asset('assets/css/vendor/bootstrap.min.css') }}">
    <link rel="stylesheet" href="{{ asset('assets/css/vendor/fontawesome.css') }}">
    <link rel="stylesheet" href="{{ asset('assets/css/vendor/lightbox.css') }}">
    <link rel="stylesheet" href="{{ asset('assets/css/plugins/plugins.css') }}">
    <link rel="stylesheet" href="{{ asset('assets/css/style.css') }}">
	
	<!-- Share Button Plugin -->
	<script type="text/javascript" src="https://platform-api.sharethis.com/js/sharethis.js#property=5ee1d33ce6d4e200124df989&product=inline-share-buttons" async="async"></script>
    <!-- <script type='text/javascript' src='https://platform-api.sharethis.com/js/sharethis.js#property=5ee1d33ce6d4e200124df989&product=inline-share-buttons&cms=website' async='async'></script> -->
    <!-- Google Platform Library -->
    <script src="https://apis.google.com/js/platform.js" async defer></script>
    
    <!-- Google AdSense -->
    <script data-ad-client="ca-pub-6072198546327745" async src="https://pagead2.googlesyndication.com/pagead/js/adsbygoogle.js"></script>
	
    <!-- Event snippet for Website lead conversion page --> 
    <script> 
        gtag('event', 'conversion', {'send_to': 'AW-599431799/i--ACIyQrdwBEPe06p0C'}); 
    </script>
	
	<!-- AMP Page -->
	<script async custom-element="amp-analytics" src="https://cdn.ampproject.org/v0/amp-analytics-0.1.js"></script>
	<!-- Global site tag (gtag) - Google Ads: 599431799 --> 
	<amp-analytics type="gtag" data-credentials="include"> 
		<script type="application/json"> { "vars": { "gtag_id": "AW-599431799", "config": { "AW-599431799": { "groups": "default" } } }, "triggers": { "C_B87_jJoZlvs": { "on": "visible", "vars": { "event_name": "conversion", "send_to": ["AW-599431799/i--ACIyQrdwBEPe06p0C"] } } } } </script> 
	</amp-analytics> 

</head>

<body>
<div id="fb-root"></div>
	<script async defer crossorigin="anonymous" src="https://connect.facebook.net/id_ID/sdk.js#xfbml=1&version=v7.0&appId=1375648166156886&autoLogAppEvents=1">
</script>

    <div class="main-page">
        @include('client.components.header')

        @yield('content')

        @include('client.components.footer')

    </div>

    <!-- JS
============================================ -->
    <!-- Modernizer JS -->
    <script src="{{ asset('assets/js/vendor/modernizr.min.js') }}"></script>
    <!-- jQuery JS -->
    <script src="{{ asset('assets/js/vendor/jquery.js') }}"></script>
    <!-- Bootstrap JS -->
    <script src="{{ asset('assets/js/vendor/bootstrap.min.js') }}"></script>
    <script src="{{ asset('assets/js/vendor/stellar.js') }}"></script>
    <script src="{{ asset('assets/js/vendor/particles.js') }}"></script>
    <script src="{{ asset('assets/js/vendor/masonry.js') }}"></script>
    <script src="{{ asset('assets/js/vendor/stickysidebar.js') }}"></script>
    <script src="{{ asset('assets/js/vendor/contactform.js') }}"></script>
    <script src="{{ asset('assets/js/plugins/plugins.min.js') }}"></script>
    <!-- Main JS -->
    <script src="{{ asset('assets/js/main.js') }}"></script>
	
    <!--Start of Tawk.to Script-->
    <script type="text/javascript">
    var Tawk_API=Tawk_API||{}, Tawk_LoadStart=new Date();
    (function(){
    var s1=document.createElement("script"),s0=document.getElementsByTagName("script")[0];
    s1.async=true;
    s1.src='https://embed.tawk.to/5efb29d84a7c6258179b9baa/default';
    s1.charset='UTF-8';
    s1.setAttribute('crossorigin','*');
    s0.parentNode.insertBefore(s1,s0);
    })();
    </script>
    <!--End of Tawk.to Script-->
    <script>
      function onSignIn(googleUser) {
        // Useful data for your client-side scripts:
        var profile = googleUser.getBasicProfile();
        console.log("ID: " + profile.getId()); // Don't send this directly to your server!
        console.log('Full Name: ' + profile.getName());
        console.log('Given Name: ' + profile.getGivenName());
        console.log('Family Name: ' + profile.getFamilyName());
        console.log("Image URL: " + profile.getImageUrl());
        console.log("Email: " + profile.getEmail());

        // The ID token you need to pass to your backend:
        var id_token = googleUser.getAuthResponse().id_token;
        console.log("ID Token: " + id_token);
      }
    </script>

</body>

</html>