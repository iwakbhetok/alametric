<div class="page-header">
    <h2 class="header-title">{{ $header_title }}</h2>
    <div class="header-sub-title">
        <nav class="breadcrumb breadcrumb-dash">
            <a href="{{ route('member.dashboard') }}" class="breadcrumb-item"><i class="anticon anticon-home m-r-5"></i>Home</a>
            <!-- <a class="breadcrumb-item" href="#">Pages</a> -->
            <span class="breadcrumb-item active">{{ $breadcrumb_item }}</span>
        </nav>
    </div>
</div>