
    <!-- Side Nav START -->
    <div class="side-nav">
        <div class="side-nav-inner">
            <ul class="side-nav-menu scrollable">
                <li class="{{{ (Request::is('member/dashboard') ? 'active' : '') }}}">
                    <a class="dropdown-toggle" href="{{ route('member.dashboard')}}">
                        <span class="icon-holder">
                            <i class="anticon anticon-dashboard"></i>
                        </span>
                        <span class="title">Dashboard</span>
                    </a>
                </li>
                <li class="nav-item dropdown">
                    <a class="dropdown-toggle" href="{{ route('member.manage') }}">
                        <span class="icon-holder">
                            <i class="anticon anticon-appstore"></i>
                        </span>
                        <span class="title">Manajemen Paket</span>
                    </a>
                </li>
                <li class="nav-item dropdown">
                    <a class="dropdown-toggle" href="{{ route('transaction.index') }}">
                        <span class="icon-holder">
                            <i class="anticon anticon-build"></i>
                        </span>
                        <span class="title">Transaksi</span>
                    </a>
                </li>
                <li class="nav-item dropdown">
                    <a class="dropdown-toggle" href="{{ route('member.affiliate')}}">
                        <span class="icon-holder">
                            <i class="anticon anticon-team"></i>
                        </span>
                        <span class="title">Program Affiliate</span>
                    </a>
                </li>
                <li class="nav-item dropdown">
                    <a class="dropdown-toggle" href="{{ route('member.news') }}">
                        <span class="icon-holder">
                            <i class="anticon anticon-form"></i>
                        </span>
                        <span class="title">Berita Pilihan</span>
                    </a>
                </li>
                <!-- <li class="nav-item dropdown">
                    <a class="dropdown-toggle" href="{{ route('member.upgrade') }}">
                        <span class="icon-holder">
                            <i class="anticon anticon-info-circle"></i>
                        </span>
                        <span class="title">Upgrade Paket</span>
                    </a>
                </li> -->
                <li class="nav-item">
                    <a class="dropdown-toggle" href="{{ route('logout') }}">
                        <span class="icon-holder">
                            <i class="anticon opacity-04 font-size-16 anticon-logout"></i>
                        </span>
                        <span class="title">Logout</span>
                    </a>
                </li>
            </ul>
        </div>
    </div>
    <!-- Side Nav END -->