@if ($posts->hasPages())
<div class="rn-pagination text-center mt--60 mt_sm--30">
    <ul class="page-list">
        @if ($posts->onFirstPage())
            <li class="disabled"></li>
        @else
            <li><a href="{{ $posts->previousPageUrl() }}" rel="prev"><i class="fas fa-angle-left"></i></a></li>
        @endif
        @for ($i = 1; $i <= $posts->lastPage(); $i++)
        <li class="{{ ($posts->currentPage() == $i) ? ' active' : '' }}">
            <a href="{{ $posts->appends(request()->except('page'))->url($i) }}">{{ $i }}</a>
        </li>
        @endfor
        @if ($posts->hasMorePages())
            <li><a href="{{ $posts->nextPageUrl() }}"><i class="fas fa-angle-right"></i></a></li>
        @else
            <li class="disabled"></li>
        @endif
        
    </ul>
</div>
@endif