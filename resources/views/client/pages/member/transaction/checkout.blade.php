@extends('client.layout.member')

@section('title',  'Checkout Paket')

@section('content')
    <div class="row">
    <div class="col-md-12">
        <!-- accordion -->
    <div class="accordion borderless" id="accordion-borderless">
        <div class="card">
            <div class="card-header">
                <h5 class="card-title">
                    <a data-toggle="collapse" href="#collapseOneBorderless">
                        <span>Your Order</span>
                    </a>
                </h5>
            </div>
            <div id="collapseOneBorderless" class="collapse show" data-parent="#accordion-borderless">
                <div class="card-body">
                    <div class="d-flex justify-content-between p-b-20 border-bottom">
                        <div class="media align-items-center">
                            <div class="avatar avatar-blue avatar-icon" style="height: 55px; width: 55px;">
                                <i class="anticon anticon-coffee font-size-25" style="line-height: 55px"></i>
                            </div>
                            <div class="m-l-15">
                                <h2 class="font-weight-bold font-size-30 m-b-0">
                                    @php
                                    if($order_detail->is_discount == 'TRUE'){
                                        $price = $order_detail->discount_price;
                                    }else{
                                        $price = $order_detail->price;
                                    }
                                    @endphp
                                    @currency($price)
                                    <span class="font-size-13 font-weight-semibold">{{ $order_detail->price_context}}</span>
                                </h2>
                                <h4 class="m-b-0">{{ Str::ucfirst($order_detail->package_name) }} Plan</h4>
                            </div>
                        </div>
                    </div>
                    {!! $order_detail->description !!}
                    <br>
                    <div class="text-center">
                        <!-- <div class="input-group">
                            <input type="text" class="form-control" placeholder="Kode promo (jika ada)">
                            <div class="input-group-append">
                            <button type="submit" class="btn btn-secondary">Redeem</button>
                            </div>
                        </div> -->
                        <hr class="mb-4">
                        <div class="row">
                            <div class="col-md-8">&nbsp;</div>
                            <div class="col-md-2 text-right">Total</div>
                            <div class="col-md-2 text-right">
                                @currency($price)
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-8">&nbsp;</div>
                            <div class="col-md-2 text-right">PPN 10%</div>
                            <div class="col-md-2 text-right">
                                @php
                                if($price != 0){
                                    $ppn = $price /100 * 10;
                                }else{
                                    $ppn = 0;
                                }
                                @endphp
                                @currency($ppn)
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-8">&nbsp;</div>
                            <div class="col-md-2 text-right">Grand Total</div>
                            <div class="col-md-2 text-right">
                                @php
                                $grandTotal = $price + $ppn;
                                @endphp
                                <b>@currency($grandTotal)</b>
                            </div>
                        </div>
                        <br>
                        <form method="POST" onsubmit="return submitForm();">
                        @csrf
                        <input type="hidden" id="order_id" name="order_id" value="{{ $order_detail->orderId }}">
                        <input type="hidden" id="amount" name="amount" value="{{ $grandTotal }}">
                        <input type="hidden" id="package_name" name="package_name" value="{{ $order_detail->package_name }}">
                        <div class="row">
                            <div class="col-md-2">
                                <a href="{{ url()->previous() }}" class="btn btn-default btn-lg btn-block m-r-5">Batal</a>
                            </div>
                            <div class="col-md-10">
                                <button class="btn btn-primary btn-lg btn-block" type="submit" onclick="">Lanjutkan Pembayaran</button>
                            </div>
                        </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
    </div>

        
    </div>
@endsection
@section('scripts')
<script
        src="https://code.jquery.com/jquery-3.3.1.min.js"
        integrity="sha256-FgpCb/KJQlLNfOu91ta32o/NMZxltwRo8QtmkMRdAu8="
        crossorigin="anonymous"></script>
    <script src="{{ !config('services.midtrans.isProduction') ? 'https://app.sandbox.midtrans.com/snap/snap.js' : 'https://app.midtrans.com/snap/snap.js' }}" data-client-key="{{ config('services.midtrans.clientKey') }}"></script>
    <script>
    function submitForm() {
        $.ajaxSetup({
		headers: {
			'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
			}
		});
        // Kirim request ajax
        $.post("{{ route('transaction.store') }}",
        {
            _method: 'POST',
            _token: '{{ csrf_token() }}',
            amount: $('#amount').val(),
            order_id: $('#order_id').val(),
            package_name:$('#package_name').val(),
        },
        function (data, status) {
            snap.pay(data.snap_token, {
                // Optional
                onSuccess: function (result) {
                    console.log("SUCCESS")
                    window.location.href = "{{ route('transaction.finish')}}";
                    // location.reload();
                },
                // Optional
                onPending: function (result) {
                    // location.reload();
                    console.log("PENDING")
                },
                // Optional
                onError: function (result) {
                    // location.reload();
                    console.log("ERROR")
                }
            });
        });
        return false;
    }
    </script>
@stop