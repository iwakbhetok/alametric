@extends('client.layout.member')

@section('title',  'Upgrade Package')

@section('content')

<div class="row align-items-center" id="monthly-view">
    @foreach($packages as $package)
            <div class="col-md-4">
                <div class="card">
                    <div class="card-body">
                        <div class="d-flex justify-content-between p-b-20 border-bottom">
                            <div class="media align-items-center">
                                <div class="avatar avatar-blue avatar-icon" style="height: 55px; width: 55px;">
                                    <i class="anticon anticon-coffee font-size-25" style="line-height: 55px"></i>
                                </div>
                                <div class="m-l-15">
                                    <h2 class="font-weight-bold font-size-22 m-b-0">
                                        @if($package->is_discount == 'TRUE')
                                        @currency($package->discount_price)
                                        @else
                                        @currency($package->price)
                                        @endif
                                        <br>
                                        <span class="font-size-13 font-weight-semibold">{{ $package->price_context }}</span>
                                    </h2>
                                    <h4 class="m-b-0">{{ $package->package_name }}</h4>
                                </div>
                            </div>
                        </div>
                        {!! $package->description !!}
                        <br>
                        <div class="text-center">
                            <a href="{{ route('transaction.create_order', ['package' => $package->slug]) }}" class="btn btn-primary">Pilih Paket</a>
                        </div>
                    </div>
                </div>
            </div>
            @endforeach
            
        </div>

@endsection