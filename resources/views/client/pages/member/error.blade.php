@extends('client.layout.member')

@section('title',  'Whoops!')

@section('content')
<h1>Whoops! Looks like you got lost</h1>
<p>We couldnt find what you were looking for.</p>

@endsection