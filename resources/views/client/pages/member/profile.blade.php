@extends('client.layout.member')

@section('title',  'Member Profile')

@section('content')

<div class="row">
    <div class="col-md-3">

    <!-- Profile Image -->
    <div class="card card-primary card-outline">
        <div class="card-body box-profile">
        <div class="text-center">
            <img class="profile-user-img img-fluid img-circle"
                src="{{ asset('member_assets/img/avatar-icon.png') }}"
                alt="User profile picture">
        </div>

        <h3 class="profile-username text-center">{{Auth::guard('member')->user()->name}}</h3>

        <p class="text-muted text-center">Alametric Member</p>

        </div>
        <!-- /.card-body -->
    </div>
    <!-- /.card -->

    </div>
    <!-- /.col -->
    <div class="col-md-9">
    <div class="card">
        <div class="card-body">
        <div class="tab-content">
            <div class="tab-pane active" id="settings">
            <form class="form-horizontal" method="post" action="{{ route('store.profile') }}">
                {{ csrf_field() }}
                <div class="form-group row">
                <label for="inputName" class="col-sm-2 col-form-label">Name</label>
                <div class="col-sm-10">
                    <input type="text" name="name" class="form-control" id="inputName" placeholder="Name" value="{{ $member->name }}">
                </div>
                <div class="col-sm-10">
                    @if($errors->has('name'))
                        <span class="help-block">
                            <strong>{{ $errors->first('name') }}</strong>
                        </span>
                    @endif
                </div>
                </div>
                <div class="form-group row">
                <label for="inputEmail" class="col-sm-2 col-form-label">Email</label>
                <div class="col-sm-10">
                    <input type="email" name="email" class="form-control" id="inputEmail" placeholder="Email" value="{{ $member->email }}">
                </div>
                <div class="col-sm-10">
                    @if($errors->has('email'))
                        <span class="help-block">
                            <strong>{{ $errors->first('email') }}</strong>
                        </span>
                    @endif
                </div>
                </div>
                <div class="form-group row">
                <label for="inputAddress" class="col-sm-2 col-form-label">Address</label>
                <div class="col-sm-10">
                    <textarea class="form-control" name="address" id="inputAddress" placeholder="Address">{{ $member->address }}</textarea>
                </div>
                </div>
                <div class="form-group row">
                <div class="offset-sm-2 col-sm-10">
                    <button type="submit" class="btn btn-danger">Submit</button>
                </div>
                </div>
            </form>
            </div>
            <!-- /.tab-pane -->
        </div>
        <!-- /.tab-content -->
        </div><!-- /.card-body -->
    </div>
    <!-- /.nav-tabs-custom -->
    </div>
    <!-- /.col -->
</div>
@endsection
