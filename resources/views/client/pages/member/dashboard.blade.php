@extends('client.layout.member')

@section('title',  'Member Dashboard')

@section('content')

    <div class="row">
        @if($order_package)
        <div class="col-md-12">
            <div class="alert alert-primary">
                <div class="d-flex justify-content-start">
                    <span class="alert-icon m-r-20 font-size-30">
                        <i class="anticon anticon-info-circle"></i>
                    </span>
                    <div>
                        <h5 class="alert-heading">Notifikasi</h5>
                        <p>Anda telah memesan paket {{ Str::ucfirst($order_package) }}, silahkan klik <a href="{{ route('transaction.create_order', ['package' => $order_package]) }}">disini</a> untuk melanjutkan.</p>
                    </div>
                </div>
            </div>
        </div>
        @endif
        @if($invalid_package)
        <div class="col-md-12">
            <div class="alert alert-warning">
                <div class="d-flex justify-content-start">
                    <span class="alert-icon m-r-20 font-size-30">
                        <i class="anticon anticon-info-circle"></i>
                    </span>
                    <div>
                        <h5 class="alert-heading">Notifikasi</h5>
                        <p>Anda belum memesan paket apapun, silahkan klik <a href="">disini</a> untuk memilih paket.</p>
                    </div>
                </div>
            </div>
        </div>
        @endif
    </div>
    <div class="row">
        <div class="col-md-6 col-lg-3">
            <div class="card">
                <div class="card-body">
                    <div class="media align-items-center">
                        <div class="avatar avatar-icon avatar-lg avatar-blue">
                            <i class="anticon anticon-star"></i>
                        </div>
                        <div class="m-l-15">
                            <h2 class="m-b-0">3</h2>
                            <p class="m-b-0 text-muted">Services</p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-md-6 col-lg-3">
            <div class="card">
                <div class="card-body">
                    <div class="media align-items-center">
                        <div class="avatar avatar-icon avatar-lg avatar-cyan">
                            <i class="anticon anticon-dollar"></i>
                        </div>
                        <div class="m-l-15">
                            <h2 class="m-b-0">12</h2>
                            <p class="m-b-0 text-muted">Billing</p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-md-6 col-lg-3">
            <div class="card">
                <div class="card-body">
                    <a href="{{ route('transaction.index') }}">
                    <div class="media align-items-center">
                        <div class="avatar avatar-icon avatar-lg avatar-gold">
                            <i class="anticon anticon-profile"></i>
                        </div>
                        <div class="m-l-15">
                            <h2 class="m-b-0">{{ $memberOrders }}</h2>
                            <p class="m-b-0 text-muted">Orders</p>
                        </div>
                    </div>
                    </a>
                </div>
            </div>
        </div>
        <div class="col-md-6 col-lg-3">
            <div class="card">
                <div class="card-body">
                    <div class="media align-items-center">
                        <div class="avatar avatar-icon avatar-lg avatar-purple">
                            <i class="anticon anticon-info-circle"></i>
                        </div>
                        <div class="m-l-15">
                            <h2 class="m-b-0">1,832</h2>
                            <p class="m-b-0 text-muted">Tickets</p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        </div>
                
    
    <div id="post_data">
    {{ csrf_field() }}
        
    </div>
    

@endsection
@section('scripts')
<script type="text/javascript">
$(document).ready(function(){
 
    var _token = $('input[name="_token"]').val();

    load_data('', _token);

 function load_data(id="", _token)
 {
    $.ajax({
    url:"{{ route('loadmore.load_post') }}",
    method:"POST",
    data:{id:id, _token:_token},
    success:function(data)
    {
        $('#load-more').remove();
        $('#post_data').append(data);
    }
    })
 }

 $(document).on('click', '#load-more', function(){
  var id = $(this).data('id');
  $('#load-more').html('<b>Loading...</b>');
  load_data(id, _token);
 });

});
</script>
@stop