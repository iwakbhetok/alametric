@extends('client.layout.member')

@section('title',  'Transaksi')

@section('content')
<!-- <h1>Fitur ini segera hadir</h1> -->

<div class="card">
    <div class="card-body">
        <h4>Daftar Transaksi</h4>
        <p>Daftar transaksi yang sudah anda lakukan</p>
        <div class="table-responsive">
        <table class="table table-hover">
            <thead>
                <tr>
                    <th scope="col">#</th>
                    <th scope="col">Nama Paket</th>
                    <th scope="col">Harga</th>
                    <th scope="col">Status</th>
                    <th scope="col">Action</th>
                </tr>
            </thead>
            <tbody>
                @php $count = 1;
                @endphp
                @foreach($orders as $item)
                <tr>
                    <th scope="row">{{ $count }}</th>
                    <td>{{ $item->package_name}}</td>
                    <td>
                        @if($item->is_discount == 'TRUE')
                            @currency($item->discount_price)
                        @else
                            @currency($item->price)
                        @endif
                         </td>
                    <td>{{ $item->order_status }}</td>
                    <th scope="row">
                        @if($item->order_status == 'PENDING')
                        <a href="{{ route('transaction.checkout', ['orderId' => $item->orderId]) }}" class="btn btn-primary">Proses Bayar</a>
                        @endif
                        @if($item->order_status == 'CANCELLED')
                        <a href="#" class="btn btn-warning">Batal</a>
                        @endif
                        @if($item->order_status == 'REFUNDED')
                        <a href="#" class="btn btn-default">Dikembalikan</a>
                        @endif
                        @if($item->order_status == 'DECLINED')
                        <a href="#" class="btn btn-danger">Ditolak</a>
                        @endif
                        @if($item->order_status == 'COMPLETED')
                        <a href="#" class="btn btn-default">Sudah Bayar</a>
                        @endif
                    </th>
                </tr>
                @php $count++;
                @endphp
                @endforeach
            </tbody>
        </table>
    </div>
    </div>
</div>

@endsection