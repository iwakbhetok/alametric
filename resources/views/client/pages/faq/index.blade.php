@extends('client.layout.app')

@section('title',  'Syarat dan Ketentuan')

@push('head')

@endpush
<script type="application/ld+json">
		{
		"@context": "https://schema.org",
		"@type": "FAQPage",
		"mainEntity": [{
			"@type": "Question",
			"name": "Apa itu Alametric?",
			"acceptedAnswer": {
			"@type": "Answer",
			"text": "Alametric.com adalah platform sosial media terintegrasi yang melayani dan menangani aktivitas media sosial anda keseluruhan baik di Facebook, Instagram, Twitter, YouTube, dan lain sebagainya."
			}
		}, {
			"@type": "Question",
			"name": "Apa itu sosial media terintegrasi?",
			"acceptedAnswer": {
			"@type": "Answer",
			"text": "Sosial media terintegrasi adalah suatu sistem yang mengalami pembauran sehingga menjadi satu kesatuan yang utuh. Dimana sosial media dikelola dengan integrasi dan koordinasi yang dapat mengoptimalkan keberadaan sosial media tersebut."
			}
		}, {
			"@type": "Question",
			"name": "Apa kelebihan menggunakan sosial media terintegrasi?",
			"acceptedAnswer": {
			"@type": "Answer",
			"text": "Anda tidak perlu repot untuk mengurus pengelolaan platform sosial media. Dengan begitu, Anda dapat menghemat waktu, memudahkan dalam penggunaan, dan terkait layanan yang diberikan Alametric sudah <strong>All in One</strong>."
			}
		}, {
			"@type": "Question",
			"name": "Siapa yang sebaiknya menggunakan layanan Alameric?",
			"acceptedAnswer": {
			"@type": "Answer",
			"text": "Siapa saja yang mempunyai bisnis ataupun individu yang ingin <strong>re-branding</strong>, UMKM, Online Shop maupun perusahaan yang membutuhkan layanan jasa sosial media yang sudah mencakup keseluruhan."
			}
		}, {
			"@type": "Question",
			"name": "Apa saja layanan yang ada di Alametric?",
			"acceptedAnswer": {
			"@type": "Answer",
			"text": "<ul><li>Sosial Media Optimasi</li><li>Optimasi Website</li><li>Digital Marketing Optimasi</li><li>Video Iklan Terintegrasi</li></ul>"
			}
		}]
		}
    </script>
@section('content')
<style>
ul.checkmark{
    
   padding-left:20px;
   text-indent:2px;
   list-style: none;
   list-style-position:outside; 
}
ul.checkmark li:before {
    content: '✔';   
    margin-left: -1em; margin-right: .100em;
}
</style>
<main class="page-wrapper">

<!-- Start Kebijakan Privasi Area  -->
<div class="rn-blog-details pt--110 pb--70 bg_color--1">
    <div class="container">
        <div class="row">
            <div class="col-lg-12">
                <div class="inner-wrapper inner">
                        <h2 class="title" style="text-align: center;">Frequently Asked Question</h2>
                        <p class="description">
                        <br/>
                        <p style="text-align: justify; font-size: 24px;">
                            <b><u>ALAMETRIC</u></b>
                        </p>
                        <p style="font-size: 20px; font-weight: bold;text-align: justify;">-  Apa itu Alametric?</p>
                        <p style="text-align: justify; ">
                            Alametric.com adalah platform sosial media terintegrasi yang melayani dan menangani aktivitas media sosial anda keseluruhan baik di Facebook, Instagram, Twitter, YouTube, dan lain sebagainya.
                        </p>
                        <br/>
						<p style="font-size: 20px; font-weight: bold;text-align: justify;">-  Apa itu sosial media terintegrasi?</p>
                        <p style="text-align: justify; ">
                            Sosial media terintegrasi adalah suatu sistem yang mengalami pembauran sehingga menjadi satu kesatuan yang utuh. Dimana sosial media dikelola dengan integrasi dan koordinasi yang dapat mengoptimalkan keberadaan sosial media tersebut.
                        </p>
						<br/>
						<p style="font-size: 20px; font-weight: bold;text-align: justify;">-  Apa kelebihan menggunakan sosial media terintegrasi?</p>
                        <p style="text-align: justify; ">
                            Anda tidak perlu repot untuk mengurus pengelolaan platform sosial media. Dengan begitu, Anda dapat menghemat waktu, memudahkan dalam penggunaan, dan terkait layanan yang diberikan Alametric sudah <i>All in One</i>.
                        </p>
						<br/>
						<p style="font-size: 20px; font-weight: bold;text-align: justify;">-  Siapa yang sebaiknya menggunakan layanan Alameric?</p>
                        <p style="text-align: justify; ">
                            Siapa saja yang mempunyai bisnis ataupun individu yang ingin <i>re-branding</i>, UMKM, Online Shop maupun perusahaan yang membutuhkan layanan jasa sosial media yang sudah mencakup keseluruhan.
                        </p>
						<br/>
						<p style="font-size: 20px; font-weight: bold;text-align: justify;">-  Mengapa harus menggunakan Alametric?</p>
                        <p style="text-align: justify; ">
                            Alametric sudah menguji terkait optimasi terbaik dan SDM yang mumpuni, unggul, dan berpengalaman. Alametric akan selalu memberikan hasil terbaik dan keberhasilan Anda menggunakan sosial media sebagai garda terdepan untuk mendapatkan pelanggan, peningkatan interaksi maupun penjualan yang diharapkan.
                        </p>
						<br/>
						<p style="font-size: 20px; font-weight: bold;text-align: justify;">-  Apa sih keuntungan menggunakan Alametric?</p>
                        <p style="text-align: justify; ">
                            Alametric memiliki <b>bundling paket komplit</b> yang dapat meningkatkan <b>Brand Awareness</b>, meningkatkan <b>keuntungan hingga 70%*</b>, serta <b>jaminan 100%</b> data Anda aman. Serta kami sudah menerapkan sistem yang mutakhir dan memupuni.
							<br/><a style="font-size: 12px;">*Dengan syarat dan ketentuan berlaku</a>
							<br/>
                            <br/>Mari kami hitungkan:
							<br/>Kami asumsikan Anda menggunakan Tim Admin, Tim Desain, Tim Digital, Marketing, Tim Copywriting di Jabodetabek. berarti Anda harus lakukan:
                        </p>
							<p style="text-align: justify; margin-left: 55px;">
                                <ul style="font-size: 16px">
									<li>Training setiap divisi dari awal</li>
									<li>Laptop/PC design dengan spesifikasi Core i5- i7 terbaru seharga Rp 7.000.000 untuk Tim Desain</li>
									<li>Handphone Admin dengan RAM 4 Gb asumsi seharga Rp 2.000.000 untuk Admin</li>
									<li>Laptop/PC Tim Copywriting dengan spesifikasi Core i3 terbaru seharga Rp 5.000.000</li>
									<li>Gaji UMR Rp 4.000.000/bulan [4 x Rp 4.000.000/bulan]</li>
									<li>Biaya internet Rp 300.000/bulan</li>
									<li>Biaya listrik Rp 350.000/ bulan dll</li>
								</ul>
                            </p>
						<p style="text-align: justify; ">
                            Estimasi biaya yang harus Anda siapkan kurang lebih <b>Rp 14.000.000</b> untuk Handphone dan Laptop khusus karyawan di awal. Dan <b>Rp 16.650.000/bulan</b> untuk penggajian dan internet 
							<br/>Nb. Belum termasuk biaya makan, transportasi, BPJS, dll
							<br/>
							<br/>Bandingkan jika Anda menggunakan Jasa kami:
                        </p>	
							<p style="text-align: justify; margin-left: 55px;">
                                <ul style="font-size: 16px">
									<li>Biaya Per bulan hanya mulai dari <b>Rp 600.000 an/bulan</b></li>
									<li>Mendapatkan insight mengenai sosial media marketing</li>
									<li>Tidak perlu training karyawan barun</li>
									<li>Tidak perlu membeli laptop khusus karyawan</li>
									<li>Tidak perlu membeli Handphone khusus karyawan</li>
									<li>Tidak perlu bayar internet</li>
									<li>Ditangani oleh Tim Profesional</li>
									<li>Free konsultasi bisnis</li>
								</ul>
                            </p>
						<p style="text-align: justify; ">
                            Jadi, pesan sekarang juga!
                        </p>
						<br/>
						<p style="font-size: 20px; font-weight: bold;text-align: justify;">-  Apa saja kelebihan Alametric dibandingkan jasa lainnya?</p>
                        <p style="text-align: justify; ">
                            Alametric dapat mencakup <b>usaha middle-up</b> dan <b>middle-low</b>. Artinya, kami terbuka untuk seluruh kalangan. Kami jauh lebih terjangkau dari segi <b>harga</b> dengan layanan yang kompleks. Alametric sudah memiliki legalitas lengkap dengan berbadan hukum PT. Untuk <b>portofolio</b>, Anda bisa cek portofolio di web kami 😁
							<br/>Kami memiliki <b>support konsultasi bisnis</b> yang dapat membantu analisis untuk setiap progress bisnis Anda, khususnya hal yang berkaitan dengan ranah Alametric. Adapun <b>hasil report bulanan</b> dan berbagai jenis data yang valid serta berguna termasuk laporan para pesaing Anda.
                        </p>
                        <br/>
						<p style="font-size: 20px; font-weight: bold;text-align: justify;">-  Apa saja layanan yang ada di Alametric?</p>
                        <p style="text-align: justify; ">
                            <ul class="checkmark" style="font-size: 18px;">
								<li>Sosial Media Optimasi</li>
								<li>Optimasi Website</li>
								<li>Digital Marketing Optimasi</li>
								<li>Video Iklan Terintegrasi</li>
							</ul>
                        </p>
						<p style="text-align: justify; ">
                            Jika ada <i>request</i> atau permintaan kebutuhan lainnya, kami siap melayani Anda dengan Paket Custom.
                        </p>
                        <br/>
						<p style="font-size: 20px; font-weight: bold;text-align: justify;">-  Apakah Alametric dapat memberikan jaminan?</p>
                        <p style="text-align: justify; ">
                            Kami memberikan garansi, jika kami tidak mengerjakan pesanan Anda, maka uang Anda akan kembali 100%.
                        </p>
                        <br/>
						<p style="font-size: 20px; font-weight: bold;text-align: justify;">-  Bagaimana dengan pajak?</p>
                        <p style="text-align: justify; ">
                            Harga yang ditampilkan sudah termasuk pajak 10% disetiap paketnya.
                        </p>
                        <br/>
						<p style="font-size: 20px; font-weight: bold;text-align: justify;">-  Apakah saya bisa <i>request</i> membuatkan foto produk?</p>
                        <p style="text-align: justify; ">
                            Jika ada foto produk maka Anda bisa kirimkan sampel produk ke kami dan belum termasuk biaya pengiriman.
                        </p>
                        <br/>
						<p style="font-size: 20px; font-weight: bold;text-align: justify;">-  Bagaimana jika bisnis saya belum memiliki website?</p>
                        <p style="text-align: justify; ">
                            Kami memiliki layanan custom website untuk bisnis Anda dengan harga bervariasi dan disesuaikan kebutuhan Anda.
                        </p>
                        <br/>
						<p style="font-size: 20px; font-weight: bold;text-align: justify;">-  Apakah saya bisa custom iklan saja?</p>
                        <p style="text-align: justify; ">
                            Bisa, Anda dapat memilih paket custom iklan.
                        </p>
                        <br/>
						<p style="font-size: 20px; font-weight: bold;text-align: justify;">-  Bagaimana mekanisme iklan?</p>
                        <p style="text-align: justify; ">
                            Kami akan menganalisa kebutuhan Anda dengan harga bervariasi. Anda dapat menghubungi customer service (CS) kami untuk berkonsultasi lebih lanjut.
                        </p>
                        <br/>
						<p style="font-size: 20px; font-weight: bold;text-align: justify;">-  Apa akun iklan menggunakan milik usaha sendiri atau Alametric?</p>
                        <p style="text-align: justify; ">
                            Akun iklan akan menggunakan milik usaha sendiri
                        </p>
                        <br/>
						<p style="font-size: 20px; font-weight: bold;text-align: justify;">-  Bagaimana mekanisme video motion?</p>
                        <p style="text-align: justify; ">
                            Kami memiliki layanan custom video motion untuk bisnis Anda dengan harga bervariasi dan disesuaikan kebutuhan Anda. Anda dapat menghubungi customer service (CS) kami untuk berkonsultasi lebih lanjut.
                        </p>
                        <br/>
						<p style="font-size: 20px; font-weight: bold;text-align: justify;">-  Bagaimana mekanisme podcast?</p>
                        <p style="text-align: justify; ">
                            Kami memiliki layanan custom video motion untuk bisnis Anda dengan harga bervariasi dan disesuaikan kebutuhan Anda. Anda dapat menghubungi customer service (CS) kami untuk berkonsultasi lebih lanjut.
                        </p>
                        <br/>
						<p style="font-size: 20px; font-weight: bold;text-align: justify;">-  Dimana podcast akan diposting?</p>
                        <p style="text-align: justify; ">
                            Podcast akan diposting melalui akun milik usaha sendiri ataupun Podcast ALA GW. Jika tidak terdapat pengisi podcats dari Anda, tim kami dapat mengisi podcast atas nama produk Anda.
                        </p>
                        <br/>
						<p style="font-size: 20px; font-weight: bold;text-align: justify;">-  Bagaimana alur pemesanan paket di Alametric?</p>
                        <p style="text-align: justify; ">
                            <ol style="font-size: 18px;">
								<li>Menentukan pilihan paket yang sesuai dengan kebutuhan Anda</li>
								<li>Melakukan pembayaran ke rekening 
									<br/><b>BNI Syariah: 0988911918
									<br/>a.n. Alametric Teknologi Asia, PT</b>
									</li>
								<li>Konfirmasi pembayaran ke CS Alametric
									<br/>dengan nomor +62 821-2360-1315 melalui WhatsApp
									</li>
								<li>Tim Alametric segera menghubungi</li>
							</ol>
                        </p>
                        <br/>
						<p style="font-size: 20px; font-weight: bold;text-align: justify;">-  Jika sudah memesan paket, bagaimana proses selanjutnya?</p>
                        <p style="text-align: justify; ">
                            Anda akan dihubungi oleh tim kami terkait dengan hal-hal dalam paket yang Anda pilih.
                        </p>
                        <br/>
						<p style="font-size: 20px; font-weight: bold;text-align: justify;">-  Kapan saya dapat melihat hasil report bulanan?</p>
                        <p style="text-align: justify; ">
                            Anda bisa mendapatkan hasil report bulanan di akhir bulan melalui e-mail ataupun WhatsApp dan nantinya bisa langsung cek di sistem atau website kami di alametric.com
                        </p>
                        <br/>
						<p style="font-size: 20px; font-weight: bold;text-align: justify;">-  Bagaimana cara agar saya bisa konsultasi bisnis?</p>
                        <p style="text-align: justify; ">
							Konsultasi bisnis bisa dijadwalkan dengan membuat appointment dengan menghubungi Customer Service (CS)
                        </p>
                        <br/>
						<p style="font-size: 20px; font-weight: bold;text-align: justify;">-  Bagaimana jika saya ada revisi terkait hasil konten yang diberikan Alametric?</p>
                        <p style="text-align: justify; ">
                            Jika <i>typo</i> ataupun ada yang miss terkait konten, maka kami akan revisi ataupun ganti. Jika ada keinginan dan hal lain, maka bisa disesuaikan saat awal konsultasi dengan sebenar-benarnya.
                        </p>
                        <br/>
						<p style="font-size: 20px; font-weight: bold;text-align: justify;">-  Bagaimana jika saya tidak ingin menggunakan Facebook Ads/Instagram Ads/Google Ads?</p>
                        <p style="text-align: justify; ">
                            Anda bisa tidak menggunakan Facebook Ads/Instagram Ads/Google Ads dalam paket.
                        </p>
                        <br/>
						<p style="font-size: 20px; font-weight: bold;text-align: justify;">-  Bisakah saya membatalkan pemesanan layanan paket yang sudah ditentukan?</p>
                        <p style="text-align: justify; ">
                            Anda dapat membatalkan pesanan. Namun, jika sudah melakukan pembayaran dan diproses oleh Alametric, tidak ada pengembalian biaya.
                        </p>
                        <br/>
						<p style="font-size: 20px; font-weight: bold;text-align: justify;">-  Bisakah saya upgrade paket yang telah dipilih?</p>
                        <p style="text-align: justify; ">
                            Tentu bisa, Anda dapat menghubungi Customer Service (CS) kami untuk konsultasi lebih lanjut.
                        </p>
                        <br/>
						<p style="font-size: 20px; font-weight: bold;text-align: justify;">-  Bagaimana saya mendapatkan layanan pengaduan kosumen?</p>
                        <p style="text-align: justify; ">
                            Anda dapat menghubungi layanan pengaduan kosumen melalui nomor WhatsApp +62 821 2360 1315
                        </p>
                        <br/>
						
                        </p>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- Start About Area  -->

</main>
@endsection