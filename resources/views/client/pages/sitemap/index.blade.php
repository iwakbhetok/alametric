<?php echo '<?xml version="1.0" encoding="UTF-8"?>'; ?>
<urlset
	xmlns="http://www.sitemaps.org/schemas/sitemap/0.9"
    xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
    xsi:schemaLocation="http://www.sitemaps.org/schemas/sitemap/0.9
    http://www.sitemaps.org/schemas/sitemap/0.9/sitemap.xsd">
	
	<url>
		<loc>{{ route('home') }}</loc>
		<priority>1.00</priority>
	</url>
	<url>
		<loc>{{ route('pricing') }}</loc>
		<priority>0.80</priority>
	</url>
	<url>
		<loc>{{ route('portfolio') }}</loc>
		<priority>0.80</priority>
	</url>
	<url>
		<loc>{{ route('blog') }}</loc>
		<priority>0.80</priority>
	</url>
	<url>
		<loc>{{ route('about') }}</loc>
		<priority>0.80</priority>
	</url>
	<url>
		<loc>{{ route('privacy.policy') }}</loc>
		<priority>0.80</priority>
	</url>
	<url>
		<loc>{{ route('faq') }}</loc>
		<priority>0.80</priority>
	</url>
	@foreach($post as $p)
		<url>
			<loc>{{ route('blog.detail', $p->slug ) }}</loc>
			<lastmod>{{ $p->created_at->toAtomString() }}</lastmod>
			<changefreq>weekly</changefreq>
			<priority>0.60</priority>
		</url>
	@endforeach

</urlset>