@extends('client.layout.app')

@section('title',  $post->title)

@push('meta')

<meta property="og:title" content="{{ $post->title }}">
<meta property="og:description" content="{{ $post->excerpt }}">
<meta property="og:image" content="{{ Voyager::image( $post->image ) }}">
<meta property="og:url" content="{{ url()->current() }}">

<meta name="twitter:title" content="{{ $post->title }}">
<meta name="twitter:description" content="{{ $post->excerpt }}">
<meta name="twitter:image" content="{{ Voyager::image( $post->image ) }}">
<meta name="twitter:card" content="summary">
@endpush

@section('content')

		<!-- Start Breadcrump Area  -->
        <div class="rn-page-title-area pt--120 pb--190 bg_image bg_image--1" data-black-overlay="8">
			<!-- <img src="{{ Voyager::image( voyager_image( $post->image, 'default')  ) }}" width="1920px" height="599px" data-black-overlay="5"> -->
            <div class="container">
                <div class="row">
                    <div class="col-lg-12">
                        <div class="blog-single-page-title text-center pt--100">
                            <h2 class="title theme-gradient">{{ $post->title }}</h2>
                                <ul class="blog-meta d-flex justify-content-center align-items-center">
                                    <li><i data-feather="clock"></i>{{ date('d M Y', strtotime($post->created_at)) }}</li>
                                    <li><i data-feather="user"></i>{{ $post->name }}</li>
									<li><i data-feather="eye"></i>{{ $post->view_counter }}</li>
                                </ul>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- End Breadcrump Area  -->
        <!-- Start Page Wrapper  -->
		<main class="page-wrapper">

            <!-- Start Blog Details Area  -->
            <div class="rn-blog-details pt--50 pb--70 bg_color--1">
                <div class="container">
                    <div class="row">
                        <div class="col-lg-12">
                            <div class="inner-wrapper">
                                <div class="inner">
                                    <p style="text-align:center; font-weight: bold; font-size: 26px;">{{ $post->excerpt }}</p>
                                    {!! $post->body !!}
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- End Blog Details Area  -->

            <!-- Start Comment Form  -->
            <div class="blog-comment-form pb--120 bg_color--1">
                <div class="container">
                    <div class="row">
                        <div class="col-lg-12">
                            <div class="inner">
								<div class="sharethis-inline-share-buttons"></div>
								<br/>
                                <h3 class="title mb--40 fontWeight500">Leave a Reply</h3>
								<div class="fb-comments" data-href="{{ Request::url() }}" data-numposts="10" data-width="100%"></div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- End Comment Form  -->

        </main>

@endsection
