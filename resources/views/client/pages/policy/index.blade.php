@extends('client.layout.app')

@section('title',  'Kebijakan Privasi')

@section('content')

<main class="page-wrapper">

            <!-- Start Kebijakan Privasi Area  -->
            <div class="rn-blog-details pt--110 pb--70 bg_color--1">
                <div class="container">
                    <div class="row">
                        <div class="col-lg-12">
                            <div class="inner-wrapper inner">
                                    <h2 class="title" style="text-align: center;">Kebijakan Privasi</h2>
                                    <p class="description">
                                    <br/>
                                    <p style="text-align: justify;">
                                        Adanya Kebijakan Privasi ini adalah komitmen nyata dari Alametric untuk menghargai dan melindungi setiap data atau informasi pribadi Pengguna situs www.alametric.com, situs-situs turunannya, serta aplikasi gawai Alametric (selanjutnya disebut sebagai "Situs").
                                    </p>
                                    <p style="text-align: justify;">
                                        Kebijakan Privasi ini (beserta syarat-syarat penggunaan dari situs Alametric sebagaimana tercantum dalam "Syarat & Ketentuan" dan informasi lain yang tercantum di Situs) menetapkan dasar atas perolehan, pengumpulan, pengolahan, penganalisisan, penampilan, pembukaan, dan/atau segala bentuk pengelolaan yang terkait dengan data atau informasi yang Pengguna berikan kepada Alametric atau yang Alametric kumpulkan dari Pengguna, termasuk data pribadi Pengguna, baik pada saat Pengguna melakukan pendaftaran di Situs, mengakses Situs, maupun mempergunakan layanan-layanan pada Situs (selanjutnya disebut sebagai "data").
                                    </p>
                                    <p style="text-align: justify;">
                                        Dengan mengakses dan/atau mempergunakan layanan Alametric, Pengguna menyatakan bahwa setiap data Pengguna merupakan data yang benar dan sah, serta Pengguna memberikan persetujuan kepada Alametric untuk memperoleh, mengumpulkan, menyimpan, mengelola dan mempergunakan data tersebut sebagaimana tercantum dalam Kebijakan Privasi maupun Syarat dan Ketentuan Alametric.
                                    </p>
									<p style="font-size: 28px; font-weight: bold;text-align: justify;">A. Perolehan dan Pengumpulan Data Pengguna</p>
									<p style="text-align: justify;">
										Alametric mengumpulkan data Pengguna dengan tujuan untuk memproses transaksi Pengguna, mengelola dan memperlancar proses penggunaan Situs, serta tujuan-tujuan lainnya. Kami menggunakan alamat email dan nomor selluer yang Anda berikan untuk mengirimkan pembaharuan atau pesan dari Situs Kami baik secara berkala maupun saat itu juga. Dalam hal tertentu Kami menggunakan informasi Anda untuk meningkatkan mutu dan pelayanan Situs. Kami tidak menjual, menyewakan dan memberikan informasi Anda kepada pihak lain. 
									</p>
									<p style="font-size: 28px; font-weight: bold;text-align: justify;">B. Pemberitahuan atau notifikasi</p>
									<p style="text-align: justify;">
										Pengunjung Alametric atau Pengguna sepakat dan bersedia untuk menerima segala notifikasi melalui media elektronik, termasuk tidak terbatas pada email, layanan pesan singkat (short messaging service atau SMS) dan/atau pesan instan (instant messaging) yang didaftarkannya pada Alametric baik mitra, vendor, anak perusahaan dan lainnya, untuk itu memberikan izin kepada Alametric untuk menghubungi Pengunjung Alametric atau Pengguna melalui media elektronik tersebut.
									</p>
									<p style="font-size: 28px; font-weight: bold;text-align: justify;">C. Pengungkapan Data Pribadi Pengguna</p>
									<p style="text-align: justify;">
										Alametric menjamin tidak ada penjualan, pengalihan, distribusi atau meminjamkan data pribadi Anda kepada pihak ketiga lain, tanpa terdapat izin dari Anda kecuali selama diizinkan oleh peraturan perundang-undangan yang berlaku dan dalam hal-hal sebagai berikut:
                                    </p>
                                    <p style="text-align: justify; margin-left: 30px;">
                                        1.  Dibutuhkan adanya pengungkapan data Pengguna kepada mitra atau pihak ketiga lain yang membantu Alametric dalam menyajikan layanan pada Situs dan memproses segala bentuk aktivitas Pengguna dalam Situs, termasuk memproses transaksi, verifikasi pembayaran dan lain-lain.
                                        <br/>2.  Alametric dapat menyediakan informasi yang relevan kepada mitra usaha sesuai dengan persetujuan Pengguna untuk menggunakan layanan mitra usaha, termasuk diantaranya aplikasi atau situs lain yang telah saling mengintegrasikan API atau layanannya, atau mitra usaha yang mana Alametric telah bekerjasama untuk mengantarkan promosi, kontes, atau layanan yang dikhususkan
                                        <br/>3.  Dibutuhkan adanya komunikasi antara mitra usaha Alametric (seperti pembayaran, dan lain-lain) dengan Pengguna dalam hal penyelesaian kendala maupun hal-hal lainnya.
                                        <br/>4.  Alametric dapat menyediakan informasi yang relevan kepada vendor, konsultan, mitra pemasaran, firma riset, atau penyedia layanan sejenis.
                                        <br/>5.  Pengguna menghubungi Alametric melalui media publik seperti blog, media sosial, dan fitur tertentu pada Situs, komunikasi antara Pengguna dan Alametric mungkin dapat dilihat secara publik.
                                        <br/>6.  Alametric dapat membagikan informasi Pengguna kepada anak perusahaan dan afiliasinya untuk membantu memberikan layanan atau melakukan pengolahan data untuk dan atas nama Alametric.
                                        <br/>7.  Alametric mengungkapkan data Pengguna dalam upaya mematuhi kewajiban hukum dan/atau adanya permintaan yang sah dari aparat penegak hukum.

									</p>
									<p style="font-size: 28px; font-weight: bold;text-align: justify;">D. Cookies</p>
									<p style="text-align: justify; margin-left: 30px;">
										1.    Cookies adalah file kecil yang secara otomatis akan mengambil tempat di dalam perangkat Pengguna yang menjalankan fungsi dalam menyimpan preferensi maupun konfigurasi Pengguna selama mengunjungi suatu situs.
                                        <br/>2.  Cookies tersebut tidak diperuntukkan untuk digunakan pada saat melakukan akses data lain yang Pengguna miliki di perangkat komputer Pengguna, selain dari yang telah Pengguna setujui untuk disampaikan.
                                        <br/>3.  Walaupun secara otomatis perangkat komputer Pengguna akan menerima cookies, Pengguna dapat menentukan pilihan untuk melakukan modifikasi melalui pengaturan browser Pengguna yaitu dengan memilih untuk menolak cookies (pilihan ini dapat membatasi layanan optimal pada saat melakukan akses ke Situs).
                                        <br/>4.  Alametric menggunakan fitur Google Analytics Demographics and Interest. Data yang kami peroleh dari fitur tersebut, seperti umur, jenis kelamin, minat Pengguna dan lain-lain, akan kami gunakan untuk pengembangan Situs dan konten Alametric. Jika tidak ingin data Anda terlacak oleh Google Analytics, Anda dapat menggunakan Add-On Google Analytics Opt-Out Browser.
                                        <br/>5.  Alametric dapat menggunakan fitur-fitur yang disediakan oleh pihak ketiga dalam rangka meningkatkan layanan dan konten Alametric, termasuk diantaranya ialah penyesuaian dan penyajian iklan kepada setiap Pengguna berdasarkan minat atau riwayat kunjungan. Jika Anda tidak ingin iklan ditampilkan berdasarkan penyesuaian tersebut, maka Anda dapat mengaturnya melalui browser.

									</p>
									<p style="font-size: 28px; font-weight: bold;text-align: justify;">E.  Penyimpanan dan Penghapusan Informasi</p>
                                    <p style="text-align: justify;">
										Alametric akan menyimpan informasi selama akun Pengguna tetap aktif dan dapat melakukan penghapusan sesuai dengan ketentuan peraturan hukum yang berlaku.
									</p>
                                    <p style="font-size: 28px; font-weight: bold;text-align: justify;">F.  Pembaruan Kebijakan Privasi</p>
                                    <p style="text-align: justify;">
                                        Alametric dapat sewaktu-waktu melakukan perubahan atau pembaruan terhadap Kebijakan Privasi ini. Alametric menyarankan agar Pengguna membaca secara seksama dan memeriksa halaman Kebijakan Privasi ini dari waktu ke waktu untuk mengetahui perubahan apapun. Dengan tetap mengakses dan menggunakan layanan Situs maupun layanan Alametric lainnya, maka Pengguna dianggap menyetujui perubahan-perubahan dalam Kebijakan Privasi.
                                    </p>
									</p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- Start About Area  -->

        </main>

@endsection