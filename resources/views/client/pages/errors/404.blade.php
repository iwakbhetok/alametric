@extends('client.layout.app')

@section('title',  'Ooops, halaman tidak ditemukan')

@section('content')

<main class="page-wrapper">
        <!-- Start 404 Page  -->
        <div class="error-page-inner bg_color--4">
            <div class="container">
                <div class="row">
                    <div class="col-lg-12">
                        <div class="inner">
                            <h1 class="title theme-gradient">404!</h1>
                            <h3 class="sub-title">HALAMAN TIDAK DITEMUKAN</h3>
                            <span>Halaman yang kamu cari tidak ditemukan</span>
                            <div class="error-button">
                                <a class="rn-button-style--2 btn_solid" href="index.php">KEMBALI KE BERANDA</a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- End 404 Page  -->
    </main>
@endsection