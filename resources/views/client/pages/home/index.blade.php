
@extends('client.layout.app')

@section('title',  'Platform Sosial Media Terintegrasi')

@section('content')
<!-- CONTENT -->

        <!-- Start Page Wrapper  -->
        <main class="page-wrapper" id="beranda">
            <!-- Start Slider Area  -->
            <div class="rn-slider-area">
                <!-- Start Single Slide  -->
                <div class="slide slide-style-1 slider-fixed--height d-flex align-items-center bg_image bg_image--1" data-black-overlay="6">
                    <div class="container position-relative">
                        <div class="row">
                            <div class="col-lg-12">
                                <div class="inner">
                                    <h1 class="title theme-gradient">Platform<br>Sosial Media <br/> Terintegrasi </h1>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- End Single Slide  -->
            </div>
            <!-- End Slider Area  -->

            <!-- Start About Area  -->
            <div class="rn-service-area rn-section-gap bg_color--1" >
                <div class="container">
                    <div class="row">

                        <!-- Start Single Service  -->
                        <div class="col-lg-4 col-md-6 col-sm-6 col-12">

                            <div class="single-service service__style--1">
                                <a href="">
                                    <div class="service">
                                        <div class="icon">
                                            <img src="{{ asset('assets/images/icons/megaphone.png') }}" alt="Creative Agency" style="height:61px;"/>
                                        </div>
                                        <div class="content">
                                            <h3 class="title"><b>Brand Awareness</b></h3>
                                            <p style="text-align: justify;">Brand awareness penting bagi bisnis karena konsumen/pelanggan cenderung membeli produk yang berasal dari merek yang dikenal.</p>
                                        </div>
                                    </div>
                                </a>
                            </div>
                        </div>
                        <!-- End Single Service  -->

                        <!-- Start Single Service  -->
                        <div class="col-lg-4 col-md-6 col-sm-6 col-12">
                            <div class="single-service service__style--1">
                                <a href="">
                                    <div class="service">
                                        <div class="icon">
                                            <img src="{{ asset('assets/images/icons/graph.png') }}" alt="Creative Agency" style="height:61px;"/>
                                        </div>
                                        <div class="content">
                                            <h3 class="title"><b>Meningkatkan Keuntungan</b></h3>
                                            <p style="text-align: justify;">Bisa meningkatkan keuntungan hingga 70%*. Dengan syarat dan ketentuan berlaku.</p>
                                        </div>
                                    </div>
                                </a>
                            </div>
                        </div>
                        <!-- End Single Service  -->

                        <!-- Start Single Service  -->
                        <div class="col-lg-4 col-md-6 col-sm-6 col-12">
                            <div class="single-service service__style--1">
                                <a href="">
                                    <div class="service">
                                        <div class="icon">
                                            <img src="{{ asset('assets/images/icons/shield.png') }}" alt="Creative Agency" style="height:61px;"/>
                                        </div>
                                        <div class="content">
                                            <h3 class="title"><b>Jaminan 100% Aman</b></h3>
                                            <p style="text-align: justify;">Setiap data customer kami akan selalu terlindungi dan bersifat rahasia termasuk data transaksi Anda.</p>
                                        </div>
                                    </div>
                                </a>
                            </div>
                        </div>
                        <!-- End Single Service  -->

                        
                    </div>
                </div>
            </div>
            <!-- Start About Area  -->

            <!-- Start Service Area  -->
            <div class="rn-service-area ptb--80 bg_image bg_image--3" id="layanan-kami">
                <div class="container">
                    <div class="row">
                        <div class="col-lg-4 col-12">
                            <div class="section-title text-left mt--30 mt_md--5 mt_mobile--5 mb_mobile--10">
                                <h2 class="title">Layanan Kami</h2>
                                <p style="text-align: justify;" >Jika Anda ada kebutuhan lainnya bisa langsung sampaikan kepada kami melalui tombol di bawah ini.</p>
                                <div class="service-btn"><a class="" href="https://wa.me/6282123601315?text=Hallo%20Alametric.com%20bisa%20bantu%20saya%20,%20saya%20ada%20kebutuhan%20lainnya%20?"><button class="btn btn-danger">
								<span class="text">Kebutuhan Lainnya</span>
								</button></a></div>
                            </div>
                        </div>
                        <div class="col-lg-8 col-12 mt_md--50">
                            <div class="row service-one-wrapper">

                                <!-- Start Single Service  -->
                                <div class="col-lg-6 col-md-6 col-sm-6 col-12">
                                    <div class="single-service service__style--4">
                                       
                                            <div class="service">
                                                <div class="icon">
                                                    <i data-feather="cast"></i>
                                                </div>
                                                <div class="content">
                                                    <h3 class="title">Sosial Media Optimasi</h3>
                                                    <p style="text-align: justify;">Tidak usah repot dan pusing buat konten serta hal lainnya, karena layanan kami sudah terintegrasi semuanya dan akan selalu dioptimasi.</p>
                                                </div>
                                            </div>
                                        </a>
                                    </div>
                                </div>
                                <!-- End Single Service  -->

                                <!-- Start Single Service  -->
                                <div class="col-lg-6 col-md-6 col-sm-6 col-12">
                                    <div class="single-service service__style--4">
                                                                                   <div class="service">
                                                <div class="icon">
                                                    <i data-feather="layers"></i>
                                                </div>
                                                <div class="content">
                                                    <h3 class="title">Optimasi Website</h3>
                                                    <p style="text-align: justify;">Jika Anda belum Go Digital. Kami akan mempersiapkan semuanya untuk Anda dan memberikan training penggunaan website-nya secara gratis.</p>
                                                </div>
                                            </div>
                                        </a>
                                    </div>
                                </div>
                                <!-- End Single Service  -->

                                <!-- Start Single Service  -->
                                <div class="col-lg-6 col-md-6 col-sm-6 col-12">
                                    <div class="single-service service__style--4">
                             
                                            <div class="service">
                                                <div class="icon">
                                                    <i data-feather="users"></i>
                                                </div>
                                                <div class="content">
                                                    <h3 class="title">Digital Marketing Optimasi</h3>
                                                    <p style="text-align: justify;">Kami selalu berusaha untuk anda dan menerapkan strategi-strategi yang dapat segera dioptimasi agar keuntungan selalu meningkat.</p>
                                                </div>
                                            </div>
                                        </a>
                                    </div>
                                </div>
                                <!-- End Single Service  -->

                                <!-- Start Single Service  -->
                                <div class="col-lg-6 col-md-6 col-sm-6 col-12">
                                    <div class="single-service service__style--4">
                                       
                                            <div class="service">
                                                <div class="icon">
                                                    <i data-feather="monitor"></i>
                                                </div>
                                                <div class="content">
                                                    <h3 class="title">Video Iklan Terintegrasi</h3>
                                                    <p style="text-align: justify;">Jika Anda belum Go Digital. Kami akan mempersiapkan semuanya untuk Anda dan memberikan training penggunaan website-nya secara gratis.</p>
                                                </div>
                                            </div>
                                        </a>
                                    </div>
                                </div>
                                <!-- End Single Service  -->

                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- Start Service Area  -->

            <!-- Start Counterup Area  -->
            <div class="rn-counterup-area pt--110 pb--110 bg_color--1">
                <div class="container">
                    <div class="row">
                        <div class="col-lg-12">
                            <div class="section-title text-center">
                                <h3 class="fontWeight500">Jadilah Bagian Dari Kami</h3>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <!-- Start Single Counterup  -->
                        <div class="col-lg-4 col-md-4 col-sm-6 col-12">
                            <div class="rn-counterup counterup_style--1">
                                <h5 class="counter ">992</h5>
                                <p class="description">Pelaku usaha UMKM</p>
                            </div>
                        </div>
                        <!-- Start Single Counterup  -->

                        <!-- Start Single Counterup  -->
                        <div class="col-lg-4 col-md-4 col-sm-6 col-12">
                            <div class="rn-counterup counterup_style--1">
                                <h5 class="counter ">575</h5>
                                <p class="description">Individu</p>
                            </div>
                        </div>
                        <!-- Start Single Counterup  -->

                        <!-- Start Single Counterup  -->
                        <div class="col-lg-4 col-md-4 col-sm-6 col-12">
                            <div class="rn-counterup counterup_style--1">
                                <h5 class="counter ">11</h5>
                                <p class="description">Perusahaan</p>
                            </div>
                        </div>
                        <!-- Start Single Counterup  -->
                    </div>
                </div>
            </div>
            <!-- End Counterup Area  -->

            <!-- Start Testimonial Area  -->
            <div class="rn-testimonial-area rn-section-gap bg_color--5" id="testi-pelanggan">
                <div class="container">
                    <div class="row">
                        <div class="col-lg-12">
                            <!-- Start Tab Content  -->
                            <div class="rn-testimonial-content tab-content" id="myTabContent">
                                <div class="tab-pane fade show active" id="" role="tabpanel" >
                                    <div class="inner">
                                        <p>1000+ Orang Lebih Pemilik Bisnis Sudah Bergabung dan Menggunakan Layanan Kami Saatnya Giliran Anda Untuk Meraih Keuntungan Berkali-Kali Lipat.</p>
									</div>
									<div class="author-info">
                                        <h3><b>Jadilah Seperti Mereka yang Sudah Membuktikannya.</b></h3>
                                    </div>
								</div>
                                <div class="tab-pane fade" id="" role="tabpanel" >
                                    <div class="inner">
                                        <p>100+ Orang Lebih Pemilik Bisnis Sudah Bergabung dan Menggunakan Layanan Kami Saatnya Giliran Anda Untuk Meraih Keuntungan Berkali-Kali Lipat.</p>
									</div>
									<div class="author-info">
                                        <h3><b>Jadilah Seperti Mereka yang Sudah Membuktikannya.</b></h3>
                                    </div>
                                </div>
                                <div class="tab-pane fade" id="" role="tabpanel" >
                                    <div class="inner">
                                        <p>100+ Orang Lebih Pemilik Bisnis Sudah Bergabung dan Menggunakan Layanan Kami Saatnya Giliran Anda Untuk Meraih Keuntungan Berkali-Kali Lipat.</p>
									</div>
									<div class="author-info">
                                        <h3><b>Jadilah Seperti Mereka yang Sudah Membuktikannya.</b></h3>
                                    </div>
                                </div>
                                <div class="tab-pane fade" id="" role="tabpanel" >
                                    <div class="inner">
                                        <p>100+ Orang Lebih Pemilik Bisnis Sudah Bergabung dan Menggunakan Layanan Kami Saatnya Giliran Anda Untuk Meraih Keuntungan Berkali-Kali Lipat.</p>
									</div>
									<div class="author-info">
                                        <h3><b>Jadilah Seperti Mereka yang Sudah Membuktikannya.</b></h3>
                                    </div>
                                </div>
                                <div class="tab-pane fade" id="" role="tabpanel" >
                                    <div class="inner">
                                        <p>100+ Orang Lebih Pemilik Bisnis Sudah Bergabung dan Menggunakan Layanan Kami Saatnya Giliran Anda Untuk Meraih Keuntungan Berkali-Kali Lipat.</p>
									</div>
									<div class="author-info">
                                        <h3><b>Jadilah Seperti Mereka yang Sudah Membuktikannya.</b></h3>
                                    </div>
                                </div>
                                <div class="tab-pane fade" id="" role="tabpanel" >
                                    <div class="inner">
                                        <p>100+ Orang Lebih Pemilik Bisnis Sudah Bergabung dan Menggunakan Layanan Kami Saatnya Giliran Anda Untuk Meraih Keuntungan Berkali-Kali Lipat.</p>
									</div>
									<div class="author-info">
                                        <h3><b>Jadilah Seperti Mereka yang Sudah Membuktikannya.</b></h3>
                                    </div>
                                </div>
                                <div class="tab-pane fade" id="" role="tabpanel" >
                                    <div class="inner">
                                        <p>100+ Orang Lebih Pemilik Bisnis Sudah Bergabung dan Menggunakan Layanan Kami Saatnya Giliran Anda Untuk Meraih Keuntungan Berkali-Kali Lipat.</p>
									</div>
									<div class="author-info">
                                        <h3><b>Jadilah Seperti Mereka yang Sudah Membuktikannya.</b></h3>
                                    </div>
                                </div>
                                <div class="tab-pane fade" id="" role="tabpanel" >
                                    <div class="inner">
                                        <p>100+ Orang Lebih Pemilik Bisnis Sudah Bergabung dan Menggunakan Layanan Kami Saatnya Giliran Anda Untuk Meraih Keuntungan Berkali-Kali Lipat.</p>
									</div>
									<div class="author-info">
                                        <h3><b>Jadilah Seperti Mereka yang Sudah Membuktikannya.</b></h3>
                                    </div>
                                </div>
                            </div>
                            <!-- End Tab Content  -->

                            <!-- Start Tab Nav  -->
                            <ul class="testimonial-thumb-wrapper nav nav-tabs" id="myTab" role="tablist">
                                <li>
                                    <a class="" id="" data-toggle="" role="" >
                                        <div class="testimonial-thumbnai">
                                            <div class="thumb">
                                                <img src="{{ asset('assets/images/client/client-1.jpg') }}" alt="Testimonial Images">
                                            </div>
                                        </div>
                                    </a>
                                </li>
                                <li>
                                    <a id="" data-toggle=""  role="" >
                                        <div class="testimonial-thumbnai">
                                            <div class="thumb">
                                                <img src="{{ asset('assets/images/client/client-2.jpg') }}" alt="Testimonial Images">
                                            </div>
                                        </div>
                                    </a>
                                </li>
                                <li>
                                    <a id="" data-toggle="" role="" >
                                        <div class="testimonial-thumbnai">
                                            <div class="thumb">
                                                <img src="{{ asset('assets/images/client/client-3.jpg') }}" alt="Testimonial Images">
                                            </div>
                                        </div>
                                    </a>
                                </li>
                                <li>
                                    <a id="" data-toggle="" role="" >
                                        <div class="testimonial-thumbnai">
                                            <div class="thumb">
                                                <img src="{{ asset('assets/images/client/client-4.jpg') }}" alt="Testimonial Images">
                                            </div>
                                        </div>
                                    </a>
                                </li>

                                <li>
                                    <a id="" data-toggle="" role="" >
                                        <div class="testimonial-thumbnai">
                                            <div class="thumb">
                                                <img src="{{ asset('assets/images/client/client-5.jpg') }}" alt="Testimonial Images">
                                            </div>
                                        </div>
                                    </a>
                                </li>

                                <li>
                                    <a id="" data-toggle="" role="" >
                                        <div class="testimonial-thumbnai">
                                            <div class="thumb">
                                                <img src="{{ asset('assets/images/client/client-9.png') }}" alt="Testimonial Images">
                                            </div>
                                        </div>
                                    </a>
                                </li>

                                <li>
                                    <a id="" data-toggle="" role="" >
                                        <div class="testimonial-thumbnai">
                                            <div class="thumb">
                                                <img src="{{ asset('assets/images/client/client-7.jpg') }}" alt="Testimonial Images">
                                            </div>
                                        </div>
                                    </a>
                                </li>

                                <li>
                                    <a id="" data-toggle="" role="" >
                                        <div class="testimonial-thumbnai">
                                            <div class="thumb">
                                                <img src="{{ asset('assets/images/client/client-8.png') }}" alt="Testimonial Images">
                                            </div>
                                        </div>
                                    </a>
                                </li>
                            </ul>
                            <!-- End Tab Content  -->
                        </div>
                    </div>
                </div>
            </div>
            <!-- Start Testimonial Area  -->

            <!-- Start Brand Area -->
            <div class="rn-brand-area brand-separation">
                <div class="container">
		<div class="row">
                        <div class="col-lg-12">
                            <div class="section-title section-title--3 text-center mb--30">
                                <h2 class="title">Klien Perusahaan</h2>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-lg-12">
                            <ul class="brand-style-2">
                                <li>
                                    <img src="{{ asset('assets/images/brand/pertamina-logo.png') }}" alt="Pertamina" />
                                </li>
                                <li>
                                    <img src="{{ asset('assets/images/brand/bpjs-ket-logo.png') }}" alt="BPJS Ketenagakerjaan" />
                                </li>
                                <li>
                                    <img src="{{ asset('assets/images/brand/kalbe-logo.png') }}" alt="Kable" />
                                </li>
                                <li>
                                    <img src="{{ asset('assets/images/brand/pelni-logo.png') }}" alt="PELNI" />
                                </li>
                                <li>
                                    <img src="{{ asset('assets/images/brand/loyalto-logo.png') }}" alt="Loyalto" />
                                </li>
                                <li>
                                    <img src="{{ asset('assets/images/brand/namastra-logo.png') }}" alt="Koperasi Namastra" />
                                </li>
                                <li>
                                    <img src="{{ asset('assets/images/brand/bank-dki-logo.png') }}" alt="Bank DKI" />
                                </li>
                                <li>
                                    <img src="{{ asset('assets/images/brand/domainesia-logo.png') }}" alt="Domainesia" />
                                </li>
                                <li>
                                    <img src="{{ asset('assets/images/brand/puskesmas-logo.png') }}" alt="Puskesmas Tangsel" />
                                </li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
            <!-- End Brand Area -->

        </main>
        <!-- End Page Wrapper  -->

@endsection