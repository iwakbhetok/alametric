@extends('client.layout.app')

@section('title',  'Lupa Password Alametric')

@section('content')


<div class="breadcrumb-area rn-bg-color ptb--55 bg_image bg_image--1" data-black-overlay="6">
</div>


<main class="page-wrapper">

    <!-- Start Contact Area  -->
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-6">
                <br/><br/>
                <div class="login-box">
                    <div class="card">
                    <div class="card-body login-card-body">
                        <p class="login-box-msg text-center">Lupa password? Masukkan email anda.</p>

                        <form action="{{ route('password.email') }}" method="post">
                        {{ csrf_field() }}
                        <div class="input-group mb-3">
                            <input type="email" class="form-control" name="email" id="email" placeholder="Email" autocomplete="off">
                            <div class="input-group-append">
                                <div class="input-group-text">
                                    <span class="fas fa-envelope"></span>
                                </div>
                            </div>
                        </div>
                        <div class="input-group mb-3">
                            @if($errors->has('email'))
                                <span class="help-block">
                                    <strong>{{ $errors->first('email') }}</strong>
                                </span>
                            @endif
                        </div>
                        <div class="row">
                            <div class="col-12">
                            <button type="submit" class="btn btn-primary btn-block" style="font-size: 18px;">Kirim</button>
                            </div>
                            <!-- /.col -->
                        </div>
                        </form>

                        <br/>
                        <a href="{{ route('login') }}" style="font-size: 14px; color: blue;">Masuk</a>
                        <br/>
                        <a href="{{ route('register') }}" class="text-center" style="font-size: 14px; color: blue;">Registrasi/Daftar</a>
                    </div>
                    <!-- /.login-card-body -->
                    </div>
                </div>
                <!-- /.login-box -->
                <br/><br/>
            </div>
        </div>
    </div>
    <!-- End Contact Area  -->
    </main>
@endsection