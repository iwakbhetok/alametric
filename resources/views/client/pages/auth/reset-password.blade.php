@extends('client.layout.app')

@section('title',  'Reset Password Alametric')

@section('content')


<div class="breadcrumb-area rn-bg-color ptb--55 bg_image bg_image--1" data-black-overlay="6">
</div>


<main class="page-wrapper">

    <!-- Start Contact Area  -->
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-6">
                <br/><br/>
                <div class="login-box">
                    <div class="card">
                    <div class="card-body login-card-body">
                        <p class="login-box-msg text-center">Masukkan password baru untuk memulihkan akun.</p>

                        <form action="{{ route('password.is_reset') }}" method="post">
                        {{ csrf_field() }}
                        <input type="hidden" name="email" value="{{ $email }}">
                        <input type="hidden" name="token" value="{{ $token }}">
                        <div class="input-group mb-3">
                            <input type="password" class="form-control" name="password" placeholder="Password" autocomplete="off">
                            <div class="input-group-append">
                            <div class="input-group-text">
                                <span class="fas fa-lock"></span>
                            </div>
                            </div>
                        </div>
                        <div class="input-group mb-3">
                            <input type="password" class="form-control" name="password_confirmation" placeholder="Confirm Password" autocomplete="off">
                            <div class="input-group-append">
                            <div class="input-group-text">
                                <span class="fas fa-lock"></span>
                            </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-12">
                            <button type="submit" class="btn btn-primary btn-block">Ubah Password</button>
                            </div>
                            <!-- /.col -->
                        </div>
                        </form>

                        <br/>
                    </div>
                    <!-- /.login-card-body -->
                    </div>
                </div>
                <!-- /.login-box -->
                <br/><br/>
            </div>
        </div>
    </div>
    <!-- End Contact Area  -->
</main>
@endsection