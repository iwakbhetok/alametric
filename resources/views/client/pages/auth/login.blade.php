@extends('client.layout.app')

@section('title',  'Masuk Alametric')

@section('content')

<div class="breadcrumb-area rn-bg-color ptb--55 bg_image bg_image--1" data-black-overlay="6">
</div>
<main class="page-wrapper">

    <!-- Start Contact Area  -->
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-6">
                <br/><br/>
                <div class="login-box">
                  <div class="card">
                    <div class="card-body login-card-body">
                      <p class="login-box-msg" style="text-align: center;font-weight:bold;">Masuk</p>

                      <form action="{{ route('login') }}" method="post">
                      {{ csrf_field() }}
                        <div class="input-group mb-3">
                          <input type="email" class="form-control" name="email" id="email" placeholder="Email" autocomplete="off">
                          <div class="input-group-append">
                            <div class="input-group-text">
                              <span class="fas fa-envelope"></span>
                            </div>
                          </div>
                        </div>
                        <div class="input-group mb-3">
                          <input type="password" class="form-control" name="password" id="password" placeholder="Password" autocomplete="off">
                          <div class="input-group-append">
                            <div class="input-group-text">
                              <span class="fas fa-lock"></span>
                            </div>
                          </div>
                        </div>
                        
                        <button type="submit" class="btn btn-primary btn-block" style="font-size: 18px;background-color:#1b1464;">Masuk</button>

                      </form>

                      <div class="social-auth-links text-center mb-3">
                        <p style="font-size: 14px;">- ATAU -</p>
                        <a href="{{ url('/auth/facebook') }}" class="btn btn-block btn-primary" style="color: #fff;">
                          <i class="fab fa-facebook mr-2"></i> Masuk dengan Facebook
                        </a>
                        <a href="{{ url('/auth/google') }}" class="btn btn-block btn-danger" style="color: #fff;">
                          <i class="fab fa-google mr-2"></i> Masuk dengan Google
                        </a>
                        <!-- <div class="g-signin2" data-onsuccess="onSignIn" data-theme="dark"></div> -->
                      </div>
                      <!-- /.social-auth-links -->

                      <!-- <a href="{{ route('password.request') }}" style="font-size: 14px; color: blue;">Lupa password</a> -->
                      <br/>
                      <a href="{{ route('register') }}" class="text-center" style="color: blue;position:absolute;left:50%;transform:translateX(-50%);" >Belum punya akun</a>
                      <br/>
                    </div>
                    <!-- /.login-card-body -->
                  </div>
                </div>
                <!-- /.login-box -->
                <br/><br/>
            </div>
        </div>
    </div>
    <!-- End Contact Area  -->
</main>

@endsection
