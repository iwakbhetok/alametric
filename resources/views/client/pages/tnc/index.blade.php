@extends('client.layout.app')

@section('title',  'Syarat dan Ketentuan')

@section('content')

<main class="page-wrapper">

<!-- Start Kebijakan Privasi Area  -->
<div class="rn-blog-details pt--110 pb--70 bg_color--1">
    <div class="container">
        <div class="row">
            <div class="col-lg-12">
                <div class="inner-wrapper inner">
                        <h2 class="title" style="text-align: center;">Terms and Condition</h2>
                        <p class="description">
                        <br/>
                        <p style="text-align: justify;">
                            Selamat datang di www.alametric.com.
                        </p>
                        <p style="text-align: justify;">
                            Syarat & ketentuan yang ditetapkan di bawah ini mengatur pemakaian jasa yang ditawarkan oleh PT Alametric Teknologi Asia terkait penggunaan situs www.Alametric.com. Pengguna disarankan membaca dengan seksama karena dapat berdampak kepada hak dan kewajiban Pengguna di bawah hukum.
                        </p>
                        <p style="text-align: justify;">
                            Dengan mendaftar dan/atau menggunakan situs www.alametric.com, maka pengguna dianggap telah membaca, mengerti, memahami dan menyetujui semua isi dalam Syarat & ketentuan. Syarat & ketentuan ini merupakan bentuk kesepakatan yang dituangkan dalam sebuah perjanjian yang sah antara Pengguna dengan PT Alametric Teknologi Asia. Jika pengguna tidak menyetujui salah satu, sebagian, atau seluruh isi Syarat & ketentuan, maka pengguna tidak diperkenankan menggunakan layanan di www.alametric.com.
                        </p>
                        <p style="font-size: 28px; font-weight: bold;text-align: justify;">A.  Definisi</p>
                        <p style="text-align: justify; ">
                            1.  PT Alametric Teknologi Asia adalah suatu perseroan terbatas yang menjalankan kegiatan usaha jasa web portal www.alametric.com, yakni situs layanan jasa yang dioperasikan oleh penjual terdaftar.
                            <br/>2.  Situs Alametric adalah www.alametric.com.
                            <br/>3.  Syarat & ketentuan adalah perjanjian antara Pengguna dan Alametric yang berisikan seperangkat peraturan yang mengatur hak, kewajiban, tanggung jawab pengguna dan Alametric, serta tata cara penggunaan sistem layanan Alametric.
                            <br/>4.  Pengguna adalah pihak yang menggunakan layanan Alametric, termasuk namun tidak terbatas pada pembeli maupun pihak lain yang sekedar berkunjung ke Situs Alametric.
                            <br/>5.  Customer atau pembeli adalah Pengguna terdaftar yang melakukan permintaan atas jasa yang dioperasionalkan oleh PT Alametric Teknologi Asia di Situs Alametric.
                            <br/>6.  Jasa adalah suatu aktivitas atau tindakan yang tidak berwujud, tidak dapat diraba tetapi dapat diidentifikasi
                            <br/>7.  Rekening Resmi Alametric adalah rekening bersama yang disepakati oleh Alametric untuk proses transaksi jual beli jasa atau layanan di Situs Alametric.
                            <br/>8.  Brand adalah kombinasi lengkap dari asosiasi yang orang bayangkan ketika mendengar sebuah nama perusahaan atau produk.

                        </p>
                        <p style="font-size: 28px; font-weight: bold;text-align: justify;">B.  Transaksi</p>
                        <p style="text-align: justify; ">
                            1. Pembeli jasa layanan wajib bertransaksi melalui prosedur transaksi yang telah ditetapkan oleh Alametric. Pembeli melakukan pembayaran dengan menggunakan metode pembayaran yang sebelumnya telah dipilih dan sistem Alametric akan mencatatnya.
                            <br/>2. Saat melakukan pembelian jasa layanan, Pembeli menyetujui bahwa:
                        </p>
                            <p style="text-align: justify; margin-left: 55px;">
                                a. Pembeli jasa layanan bertanggung jawab untuk membaca, memahami, dan menyetujui informasi/deskripsi keseluruhan jasa layanan (fungsi dan lainnya) sebelum membuat tawaran atau komitmen untuk membeli.
                                <br/>b. Pengguna masuk ke dalam kontrak yang mengikat secara hukum untuk membeli jasa layanan ketika Pengguna membeli suatu jasa layanan.
                                <br/>c. Alametric tidak mengalihkan kepemilikan secara hukum atas jasa layanan dari satu brand ke brand lainnya.
                            </p>
                        <p style="text-align: justify; ">
                            3. Alametric memiliki kewenangan sepenuhnya untuk menolak pembayaran tanpa pemberitahuan terlebih dahulu.
                            <br/>4. Konfirmasi pembayaran dengan setoran tunai wajib disertai dengan berita pada slip setoran berupa nomor invoice dan nama. Konfirmasi pembayaran dengan setoran tunai tanpa keterangan tidak akan diproses oleh Alametric.
                            <br/>5. Pembeli jasa layanan menyetujui untuk tidak memberitahukan atau menyerahkan bukti pembayaran dan/atau data pembayaran kepada pihak lain selain Alametric. Dalam hal terjadi kerugian akibat pemberitahuan atau penyerahan bukti pembayaran dan/atau data pembayaran oleh Pembeli jasa layanan kepada pihak lain, maka hal tersebut akan menjadi tanggung jawab Pembeli.
                            <br/>6. Pembeli wajib melakukan konfirmasi pembayaran dalam batas waktu 2 (dua) hari setelah pembeli melakukan pembayaran. Jika dalam batas waktu tersebut tidak ada konfirmasi atau klaim dari pihak Pembeli, maka pembelian jasa layanan tidak dapat diproses atau ditangguhkan dan uang tidak dapat di refund atau dikembalikan seutuhnya atau sebagian.
                            <br/>7. Pembeli memahami dan menyetujui bahwa masalah keterlambatan proses pembayaran dan biaya tambahan yang disebabkan oleh perbedaan bank yang Pembeli pergunakan dengan bank Rekening resmi Alametric adalah tanggung jawab Pembeli secara pribadi.
                            <br/>8. Pengembalian dana dari Alametric kepada Pembeli hanya dapat dilakukan jika dalam keadaan-keadaan tertentu berikut ini:
                        </p>
                            <p style="text-align: justify; margin-left: 55px;">
                                a.  Kelebihan pembayaran dari Pembeli jasa layanan,
                                <br/>b. Alametric sudah menyanggupi order layanan, tetapi setelah batas waktu yang ditentukan ternyata Alametric tidak mengerjakan atas jasa yang dipesan oleh Pembeli Jasa layanan hingga batas waktu yang telah ditentukan.
                                <br/>c. Penyelesaian permasalahan melalui Pusat Bantuan berupa keputusan untuk pengembalian dana kepada Pembeli atau hasil keputusan dari pimpinan PT Alametric Teknologi Asia.
                            </p>
                        <p style="text-align: justify; ">
                            9. Apabila terjadi proses pengembalian dana, maka pengembalian akan dilakukan melalui Refund milik Pengguna yang akan bertambah sesuai dengan jumlah pengembalian dana. Jika Pengguna menggunakan pilihan metode pembayaran kartu kredit maka pengembalian dana akan merujuk pada ketentuan bagian Kartu Kredit.
                            <br/>10. Alametric berwenang mengambil keputusan atas permasalahan-permasalahan transaksi yang belum terselesaikan akibat tidak adanya kesepakatan penyelesaian, dengan melihat dan menunjukkan bukti-bukti yang ada. Keputusan pimpinan PT Alametric Teknologi Asia adalah keputusan akhir yang tidak dapat diganggu gugat dan mengikat pihak Pembeli dan pembeli jasa harus mematuhinya.
                            <br/>11. Apabila Pembeli memilih menggunakan metode pembayaran transfer bank, maka total pembayaran akan dikembalikan ke Pembeli setelah menunjukkan bukti-bukti yang ada dan sah.
                            <br/>12. Pembeli wajib melakukan pembayaran dengan nominal yang sesuai dengan jumlah tagihan beserta kode unik (apabila ada) yang tertera pada halaman pembayaran. PT Alametric Teknologi Asia tidak bertanggungjawab atas kerugian yang dialami Pembeli apabila melakukan pembayaran yang tidak sesuai dengan jumlah tagihan yang tertera pada halaman pembayaran.

                        </p>
                        <p style="font-size: 28px; font-weight: bold;text-align: justify;">C.  Kartu Kredit</p>
                        <p style="text-align: justify; ">
                            1. Pengguna dapat memilih untuk mempergunakan pilihan metode pembayaran menggunakan kartu kredit untuk transaksi pembelian jasa layanan digital melalui Situs Alametric.
                            <br/>2. Transaksi pembelian jasa layanan dengan menggunakan kartu kredit wajib mengikuti syarat dan ketentuan yang diatur oleh Alametric.
                            <br/>3. Pengguna dilarang untuk mempergunakan metode pembayaran kartu kredit di luar peruntukan sebagai alat pembayaran.
                            <br/>4. Apabila terdapat transaksi dengan menggunakan kartu kredit yang melanggar ketentuan hukum dan/atau syarat ketentuan Alametric, maka Alametric berwenang untuk:
                        </p >
                            <p style="text-align: justify; margin-left: 55px;">
                                a. menahan dana transaksi selama diperlukan oleh Alametric, pihak Bank, maupun mitra payment gateway terkait untuk melakukan investigasi yang diperlukan, sekurang-kurangnya 14 (empat belas) hari.
                                <br/>b. melakukan pemotongan dana sebesar 15% (lima belas persen) dari nilai transaksi, serta menarik kembali nilai subsidi sehubungan penggunaan kartu kredit.
                            </p>
                        <p style="text-align: justify; ">
                            5. Apabila transaksi pembelian jasa layanan tidak berhasil dan/atau dibatalkan, maka tagihan atas transaksi tersebut akan dibatalkan dan dana transaksi akan dikembalikan ke limit kartu kredit pembeli di tagihan berikutnya. Ketentuan pada ayat ini tidak berlaku untuk transaksi pembelian jasa layanan dengan menggunakan kartu kredit yang melanggar ketentuan hukum dan/atau syarat ketentuan Alametric.
                            <br/>6. Transaksi pembelian yang menggunakan metode pembayaran cicilan kartu kredit akan dikenakan biaya layanan yang diberlakukan untuk tujuan pemeliharaan sistem dan peningkatan layanan dalam bertransaksi melalui Alametric, dengan rincian sebagai berikut:
                        </p>
                            <p style="text-align: justify; margin-left: 55px;">
                                i. Cicilan 3 (tiga) bulan sebesar 2.5% (dua koma lima persen)
                                <br/>ii. Cicilan 6 (enam) bulan sebesar 3.5% (tiga koma lima persen)
                                <br/>iii.   Cicilan 12 (dua belas) bulan sebesar 6% (enam persen)
                            </p>
                        <p style="text-align: justify; ">
                            7. Apabila seluruh transaksi dalam satu pembayaran yang menggunakan kartu kredit dibatalkan, maka biaya layanan tersebut akan dikembalikan ke limit kartu kredit Pengguna. Namun apabila hanya sebagian transaksi dalam satu pembayaran yang menggunakan kartu kredit dibatalkan, maka biaya layanan tersebut tidak akan ikut serta dikembalikan.

                        </p>
                        <p style="font-size: 28px; font-weight: bold;text-align: justify;">D.  Komisi</p>
                        <p style="text-align: justify; ">
                            1. Alametric tidak mengenakan komisi untuk transaksi yang dilakukan melalui Situs Alametric, kecuali apabila Pengguna menggunakan fitur atau layanan tertentu.
                            <br/>2. Apabila dikemudian hari akan diberlakukan sistem komisi dalam transaksi melalui Situs Alametric maka akan diberitahukan kepada Pengguna yang terkena dampaknya melalui Situs Alametric.

                        </p>
                        <p style="font-size: 28px; font-weight: bold;text-align: justify;">E.  Harga</p>
                        <p style="text-align: justify;">
                            1. Harga paket yang terdapat dalam situs Alametric adalah harga yang ditetapkan oleh Alametric. 
                            <br/>2. Pembeli jasa layanan memahami dan menyetujui bahwa kesalahan keterangan harga dan informasi lainnya yang disebabkan tidak terbaharuinya halaman situs Alametric dikarenakan browser/ISP yang dipakai Pembeli adalah tanggung jawab Pembeli.
                            <br/>3. Pengguna memahami dan menyetujui bahwa setiap masalah dan/atau perselisihan yang terjadi akibat ketidaksepahaman tentang harga bukanlah merupakan tanggung jawab Alametric.
                            <br/>4. Dengan melakukan pemesanan melalui Alametric, Pengguna menyetujui untuk membayar total biaya yang harus dibayarkan sebagaimana tertera dalam halaman pembayaran, yang terdiri dari harga paket layanan, pajak dan biaya-biaya lain yang mungkin timbul dan akan diuraikan secara tegas dalam halaman pembayaran. Pengguna setuju untuk melakukan pembayaran melalui metode pembayaran yang telah dipilih sebelumnya oleh Pengguna.
                            <br/>5. Peningkatan keuntungan yang berada dalam informasi situs Alametric adalah estimasi dan tidak bersifat mengikat serta fitur yang diberikan tanda * tidak termsuk dalam spend untuk iklan atau tidak termasuk modal untuk iklannya, sehingga uang untuk iklan berasal dari pembeli jasa layanan.
                        </p>
                        <p style="font-size: 28px; font-weight: bold;text-align: justify;">F.  Konten</p>
                        <p style="text-align: justify;">
                            1. Dalam menggunakan setiap fitur dan/atau layanan Alametric, Pengguna dilarang untuk memesan atau menginstruksikan mempergunakan kata-kata, komentar, gambar, atau konten apapun yang mengandung unsur SARA, diskriminasi, merendahkan atau menyudutkan orang lain, vulgar, bersifat ancaman, beriklan atau melakukan promosi ke situs selain Situs Alametric, atau hal-hal lain yang dapat dianggap tidak sesuai dengan nilai dan norma sosial maupun berdasarkan kebijakan yang ditentukan sendiri oleh Alametric. Alametric berhak melakukan tindakan yang diperlukan atas pelanggaran ketentuan ini, antara lain penghapusan konten, pemblokiran akun, dan lain-lain.
                            <br/>2. Konten atau materi yang akan ditampilkan atau ditayangkan pada Situs/Aplikasi sosial media melalui Feed atau lainnya akan tunduk pada Ketentuan Situs, peraturan hukum, serta etika pariwara yang berlaku.
                        </p>
                        <p style="font-size: 28px; font-weight: bold;text-align: justify;">G.  Promo</p>
                        <p style="text-align: justify;">
                            1. Alametric sewaktu-waktu dapat mengadakan kegiatan promosi (selanjutnya disebut sebagai “Promo”) dengan Syarat dan Ketentuan yang mungkin berbeda pada masing-masing kegiatan Promo. Pengguna dihimbau untuk membaca dengan seksama Syarat dan Ketentuan Promo tersebut.
                            <br/>2. Alametric berhak, tanpa pemberitahuan sebelumnya, melakukan tindakan-tindakan yang diperlukan termasuk namun tidak terbatas pada menarik subsidi atau cashback, membatalkan benefit, pencabutan Promo, membatalkan transaksi, menahan dana, atau menutup akun, serta hal-hal lainnya jika ditemukan adanya manipulasi, pelanggaran maupun pemanfaatan Promo untuk keuntungan pribadi Pengguna, maupun indikasi kecurangan atau pelanggaran pelanggaran Syarat dan Ketentuan Alametric dan ketentuan hukum yang berlaku di wilayah negara Indonesia.
                            <br/>3. Pengguna hanya boleh menggunakan 1 (satu) akun Alametric untuk mengikuti setiap promo Alametric. Jika ditemukan pembuatan lebih dari 1 (satu) akun oleh 1 (satu) Pengguna yang mempunyai informasi akun yang sama dan/atau identitas yang sama, maka Pengguna tidak berhak mendapatkan manfaat dari promo Alametric.
                        </p>
                        <p style="font-size: 28px; font-weight: bold;text-align: justify;">H.  Ketentuan Lain</p>
                        <p style="text-align: justify;">
                            1. Apabila Pengguna mempergunakan fitur-fitur yang tersedia dalam situs Alametric maka Pengguna dengan ini menyatakan memahami dan menyetujui segala syarat dan ketentuan.
                            <br/>2. Segala hal yang belum dan/atau tidak diatur dalam syarat dan ketentuan khusus dalam fitur tersebut maka akan sepenuhnya merujuk pada syarat dan ketentuan Alametric secara umum.
                        </p>
                        <p style="font-size: 28px; font-weight: bold;text-align: justify;">I.  Penolakan Jaminan Dan Batasan Tanggung Jawab</p>
                        <p style="text-align: justify;">
                            Alametric adalah portal web dengan model Business to Customer, yang menyediakan layanan kepada Pengguna untuk dapat mebeli jasa layanan yang ada di website Alametric.
                        </p>
                        <p style="text-align: justify;">
                            Alametric selalu berupaya untuk menjaga Layanan Alametric aman, nyaman, dan berfungsi dengan baik, tapi kami tidak dapat menjamin operasi terus-menerus atau akses ke Layanan kami dapat selalu sempurna. Informasi dan data dalam situs Alametric memiliki kemungkinan tidak terjadi secara real time.
                        </p>
                        <p style="text-align: justify;">
                            Pengguna setuju bahwa Anda memanfaatkan jasa Layanan Alametric atas risiko Pengguna sendiri, dan Layanan Alametric diberikan kepada Anda pada "SEBAGAIMANA ADANYA" dan "SEBAGAIMANA TERSEDIA".
                        </p>
                        <p style="text-align: justify;">
                            Sejauh diizinkan oleh hukum yang berlaku, Alametric (termasuk Induk Perusahaan, direktur, dan karyawan) adalah tidak bertanggung jawab, dan Anda setuju untuk tidak menuntut Alametric bertanggung jawab, atas segala kerusakan atau kerugian (termasuk namun tidak terbatas pada hilangnya uang, reputasi, keuntungan, atau kerugian tak berwujud lainnya) yang diakibatkan secara langsung atau tidak langsung.
                        </p>
                        <p style="font-size: 28px; font-weight: bold;text-align: justify;">J.  Ganti Rugi</p>
                        <p style="text-align: justify;">
                            Pengguna akan melepaskan Alametric dari tuntutan ganti rugi dan menjaga alametric (termasuk Induk Perusahaan, direktur, dan karyawan) dari setiap klaim atau tuntutan, termasuk biaya hukum yang wajar, yang dilakukan oleh pihak ketiga yang timbul dalam hal Anda melanggar Perjanjian ini, penggunaan Layanan alametric yang tidak semestinya dan/ atau pelanggaran Anda terhadap hukum atau hak-hak pihak ketiga.
                        </p>
                        <p style="font-size: 28px; font-weight: bold;text-align: justify;">K.  Pilihan Hukum</p>
                        <p style="text-align: justify;">
                            Perjanjian ini akan diatur oleh dan ditafsirkan sesuai dengan hukum Republik Indonesia, tanpa memperhatikan pertentangan aturan hukum. Anda setuju bahwa tindakan hukum apapun atau sengketa yang mungkin timbul dari, berhubungan dengan, atau berada dalam cara apapun berhubungan dengan situs dan/atau Perjanjian ini akan diselesaikan secara eksklusif dalam yurisdiksi pengadilan Republik Indonesia.
                        </p>
                        <p style="font-size: 28px; font-weight: bold;text-align: justify;">L.  Pembaharuan</p>
                        <p style="text-align: justify;">
                            Syarat & ketentuan mungkin diubah dan/atau diperbaharui dari waktu ke waktu tanpa pemberitahuan sebelumnya. Alametric menyarankan agar anda membaca secara seksama dan memeriksa halaman Syarat & ketentuan ini dari waktu ke waktu untuk mengetahui perubahan apapun. Dengan tetap mengakses dan menggunakan layanan Alametric, maka pengguna dianggap menyetujui perubahan-perubahan dalam Syarat & Ketentuan.
                        </p>
                        </p>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- Start About Area  -->

</main>
@endsection