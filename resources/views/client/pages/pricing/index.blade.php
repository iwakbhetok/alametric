
@extends('client.layout.app')

@section('title',  'Platform Sosial Media Terintegrasi')

@section('content')
		
		<!-- Start Breadcrump Area  -->
        <div class="breadcrumb-area rn-bg-color ptb--120 bg_image bg_image--1" data-black-overlay="6">
            <div class="container">
                <div class="row">
                    <div class="col-lg-12">
                        <div class="breadcrumb-inner pt--100 pt_sm--40 pt_md--50">
                            <h2 class="title">Harga</h2>
                            <ul class="page-list">
                                <li><a href="{{ route('home') }}">Home</a></li>
                                <li class="current-page">Harga</li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- End Breadcrump Area  -->
		<!-- Start Page Wrapper  -->
		<main class="page-wrapper">
			<!-- Start PROGRAM KHUSUS Area -->
            <div class="rn-pricing-plan-area rn-section-gap bg_color--1">
                <div class="container">
					<div class="row">
                        <div class="col-lg-12">
                            <div class="section-title section-title--3 text-center mb--30">
                                <!-- <h2 class="title">Program Bakti Negeri </h2>
								<p>Dikarenakan adanya dampak COVID-19 di Indonesia. Kami menyediakan paket subsidi khusus untuk UMKM.</p> -->
                            </div>
                        </div>
                    </div>
                    <div class="row mt_dec--30">						
                        <div class="col-lg-4 col-md-6 col-12 mt--30">
                            <div class="rn-pricing">
                                <div class="pricing-table-inner">
                                    <div class="pricing-header">
                                        <h3 class="title">Gratis</h3>
                                        <div class="pricing">
											<span><p style="font-size: 28px; color: #adadad; font-weight: bold; text-decoration: line-through;"></p></span>
                                            <br/>
                                            <span class="price" style="font-size: 42px; font-weight: bold;">Rp.0</span>
                                            <span class="subtitle"><br>Gratis Selamanya</span>
                                        </div>
                                    </div>
                                    <div class="pricing-body">
                                        <ul class="list-style--1" style="text-align: left;">
                                            <li><i class="fas fa-check" style="color:green;"></i>Insight Berwirausaha*</li>
                                            <li><i class="fas fa-check" style="color:green;"></i>Affiliate Marketing Program*</li>
                                            <li><i class="fas fa-check" style="color:green;"></i>Iklan di Alametric Media</li>
                                            <li><i class="fas fa-times"></i>Social Media Maintenance</li>
                                            <li><i class="fas fa-times"></i>Photography/Design Content Service</li>
                                            <li><i class="fas fa-times"></i>Laporan Bulanan</li>
                                            <li>&nbsp;</li>
                                            <li>&nbsp;</li>
                                            <li>&nbsp;</li>
                                            <li>&nbsp;</li>
                                            <li>&nbsp;</li>
                                        </ul>
                                    </div>
                                    <div class="pricing-footer">
										 <br/><br/>
                                        <a class="rn-btn" href="{{ route('register') }}">Daftar Gratis</a>
                                        <br/><br/>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="col-lg-4 col-md-6 col-12 mt--30">
                            <div class="rn-pricing ">
                                <div class="pricing-table-inner">
                                    <div class="pricing-header">
                                        <h3 class="title">Aplikasi WA (WhatsApp)</h3>
                                        <div class="pricing">
                                            <span><p style="font-size: 28px; color: #adadad; font-weight: bold; text-decoration: line-through;">Rp. 500.000</p></span>
                                            <br/>
                                            <span class="price" style="font-size: 42px; font-weight: bold;">Rp 255.000</span>
                                            <span class="subtitle">Promo (Untuk 10 Pemesan)</span>
                                        </div>
                                    </div>
                                    <div class="pricing-body">
                                        <ul class="list-style--1" style="text-align: left;">
                                            <li><i class="fas fa-check" style="color:green;"></i>Unlimited Akun No. WhatsApp</li>
                                            <li><i class="fas fa-check" style="color:green;"></i>Import & Export Kontak File (.CSV | .TXT)</li>
                                            <li><i class="fas fa-check" style="color:green;"></i>Pengiriman Text, Foto (Gambar), File (.DOCX | .CSV | .PDF | DLL)</li>
                                            <li><i class="fas fa-check" style="color:green;"></i>Fitur Terbaru (Personalisasi Akun)</li>
                                            <li><i class="fas fa-check" style="color:green;"></i>Support Jeda Waktu (Anti-Banned)</li>
                                            <li><i class="fas fa-check" style="color:green;"></i>Ambil Kontak Dari Grup (Update)</li>
                                            <li><i class="fas fa-check" style="color:green;"></i>Verivikasi Nomor WhatsApp (Aktif | Non Aktif)</li>
                                            <li><i class="fas fa-check" style="color:green;"></i>Atur Format Pesan (Message)</li>
                                            <li><i class="fas fa-check" style="color:green;"></i>Bot Customer Service / Auto Reply</li>
                                            <li><i class="fas fa-check" style="color:green;"></i>Schedule Post</li>
                                        </ul>
                                    </div>
                                    <div class="pricing-footer">
                                        <a class="rn-btn" href="http://wppredirect.tk/go/?p=6282123601315&m=Hai%20Alametric,%20Saya%20tertarik%20dengan%20paket%20Aplikasi%20WA">Order Sekarang</a>
                                        <br/><br/>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="col-lg-4 col-md-6 col-12 mt--30">
                            <div class="rn-pricing">
                                <div class="pricing-table-inner">
                                    <div class="pricing-header">
                                        <h3 class="title">Custom</h3>
                                        <div class="pricing">
                                            <span><p style="font-size: 28px; color: #adadad; font-weight: bold; text-decoration: line-through;"></p></span>
                                            <br/>
                                            <!-- <span class="price" style="font-size: 42px; font-weight: bold;">&nbsp;</span>
                                            <span class="subtitle">&nbsp;</span> -->
                                        </div>
                                    </div>
                                     <div class="pricing-body">
                                        <ul class="list-style--1" style="text-align: left;">
                                            <li>&nbsp;</li>
                                            <li style="text-align: center;"><i class="" style="text-align: center;"></i>Informasi lebih lanjut dengan menekan tombol di bawah ini untuk mendapatkan sesuai keinginan Anda.</li>
                                            <li>&nbsp;</li>
                                            <li>&nbsp;</li>
                                            <li>&nbsp;</li>
                                            <li>&nbsp;</li>
                                            <li>&nbsp;</li>
                                            <li>&nbsp;</li>
                                            <li>&nbsp;</li>
                                            <li>&nbsp;</li>
                                            <li>&nbsp;</li>
                                            <li>&nbsp;</li>
                                            <li>&nbsp;</li>
                                        </ul>
                                    </div>
                                    <div class="pricing-footer">
                                        <a class="rn-btn" href="http://wppredirect.tk/go/?p=6282123601315&m=Hai%20Alametric,%20Saya%20tertarik%20dengan%20paket%20Custom.">Kontak Kami</a>
                                        <br/><br/>
                            
                                    </div>
                                </div>
                            </div>
                        </div>
						
							
                    </div>
                </div>
            </div>
            <!-- End Brand Area -->
			
        </main>
		
		<!-- Modal -->
		<div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
		  <div class="modal-dialog" role="document">
			<div class="modal-content">
			  <div class="modal-header bg-warning" >
				<h5 class="modal-title" id="exampleModalLabel">Perhatian</h5>
				<button type="button" class="close" data-dismiss="modal" aria-label="Close">
				  <span aria-hidden="true">&times;</span>
				</button>
			  </div>
			  <div class="modal-body">
				<p style="text-align:center">Mohon Maaf, Kuota Penuh!</p>
			  </div>
			  <div class="modal-footer">
				<button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
			  </div>
			</div>
		  </div>
		</div>
		
@endsection