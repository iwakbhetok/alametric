<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreatePackagesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('packages', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->integer('package_category_id');
            $table->string('package_name')->nullable();
            $table->text('description')->nullable();
            $table->string('slug')->unique();
            $table->string('price')->default('0');
            $table->string('discount_price')->default('0');
            $table->enum('is_discount', ['TRUE', 'FALSE'])->default('FALSE');
            $table->string('price_context')->default('/ Bulan');
            $table->string('cta_url')->nullable();
            $table->enum('status', ['ACTIVE', 'INACTIVE'])->default('ACTIVE');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('packages');
    }
}
