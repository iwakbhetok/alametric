<?php

use Illuminate\Database\Seeder;

class PackageCategoriesSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('package_categories')->insert(
            [
                [
                    'category_name' => 'Program Bakti Negeri',
                    'description' => 'Dikarenakan adanya dampak COVID-19 di Indonesia. Kami menyediakan paket subsidi khusus untuk UMKM.',
                    'slug' => 'program-bakti-negeri',
                    'status' => 'ACTIVE',
                    'created_at' => date('Y-m-d H:i:s'),
                    'updated_at' => date('Y-m-d H:i:s'),
                ],
                [
                    'category_name' => 'Paket Bisnis',
                    'description' => 'Langkah yang tepat melakukan pertumbuhan & pengembangan.',
                    'slug' => 'paket-bisnis',
                    'status' => 'ACTIVE',
                    'created_at' => date('Y-m-d H:i:s'),
                    'updated_at' => date('Y-m-d H:i:s'),
                ]
            ]);
    }
}
