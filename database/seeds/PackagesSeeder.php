<?php

use Illuminate\Database\Seeder;

class PackagesSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('packages')->insert([
        	[
                'package_category_id' => '1',
                'package_name' => 'Gratis',
                'description' => '',
                'slug' => 'gratis',
                'price' => '0',
                'discount_price' => '0',
                'is_discount' => 'FALSE',
                'price_context' => 'Gratis Selamanya',
                'cta_url' => '',
                'status' => 'ACTIVE',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => date('Y-m-d H:i:s'),
            ],
            [
                'package_category_id' => '1',
                'package_name' => 'Basic',
                'description' => '',
                'slug' => 'basic',
                'price' => '569780',
                'discount_price' => '375890',
                'is_discount' => 'TRUE',
                'price_context' => '/ Bulan',
                'cta_url' => '',
                'status' => 'ACTIVE',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => date('Y-m-d H:i:s'),
            ],
            [
                'package_category_id' => '1',
                'package_name' => 'Survival',
                'description' => '',
                'slug' => 'survival',
                'price' => '1499870',
                'discount_price' => '689900',
                'is_discount' => 'TRUE',
                'price_context' => '/ Bulan',
                'cta_url' => '',
                'status' => 'ACTIVE',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => date('Y-m-d H:i:s'),
            ],
            [
                'package_category_id' => '2',
                'package_name' => 'Professional',
                'description' => '',
                'slug' => 'professional',
                'price' => '4789900',
                'discount_price' => '3097980',
                'is_discount' => 'TRUE',
                'price_context' => '/ Bulan',
                'cta_url' => '',
                'status' => 'ACTIVE',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => date('Y-m-d H:i:s'),
            ],
            [
                'package_category_id' => '2',
                'package_name' => 'Business',
                'description' => '',
                'slug' => 'business',
                'price' => '8129900',
                'discount_price' => '5198900',
                'is_discount' => 'TRUE',
                'price_context' => '/ Bulan',
                'cta_url' => '',
                'status' => 'ACTIVE',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => date('Y-m-d H:i:s'),
            ],
            [
                'package_category_id' => '2',
                'package_name' => 'Advanced',
                'description' => '',
                'slug' => 'advanced',
                'price' => '10289900',
                'discount_price' => '8189900',
                'is_discount' => 'TRUE',
                'price_context' => '/ Bulan',
                'cta_url' => '',
                'status' => 'ACTIVE',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => date('Y-m-d H:i:s'),
            ]
        ]);
    }
}
