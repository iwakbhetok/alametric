<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

use Illuminate\Support\Facades\Route;
use TCG\Voyager\Facades\Voyager;

Route::get('/', 'PagesController@index')->name('home');
Route::get('/home', 'PagesController@index')->name('home');
Route::get('/harga', 'PagesController@pricing')->name('pricing');
Route::get('/portfolio', 'PagesController@portfolio')->name('portfolio');
Route::get('/media', 'BlogsController@index')->name('blog');
Route::get('/media/{slug}', 'BlogsController@show')->name('blog.detail');
Route::get('/tentang-kami', 'PagesController@about')->name('about');
Route::get('/kebijakan-privasi', 'PagesController@policy')->name('privacy.policy');
Route::get('/terms-condition', 'PagesController@tnc')->name('terms.condition');
Route::get('/faq', 'PagesController@faq')->name('faq');
Route::get('/sitemap.xml', 'SitemapController@index');

// hanya untuk guest yg belum auth
Route::get('/register', 'Auth\RegisterController@getRegister')->name('register');
Route::post('/register', 'Auth\RegisterController@store')->name('store');

Route::get('/signin', 'Auth\LoginController@getReLogin')->name('relogin');
Route::get('/login', 'Auth\LoginController@getLogin')->name('login');
Route::post('/login', 'Auth\LoginController@postLogin');
Route::get('/logout', 'Auth\LoginController@logout')->name('logout');


Route::get('/password/reset', 'Auth\ForgotPasswordController@showLinkRequestForm')->name('password.request');
Route::post('/password/email', 'Auth\ForgotPasswordController@sendResetLinkEmail')->name('password.email');

Route::get('/password/reset/{token}', 'Auth\ResetPasswordController@showResetForm')->name('password.reset');
Route::post('/password/reset', 'Auth\ResetPasswordController@resetPassword')->name('password.is_reset');

// Route::get('/member', 'MemberController@dashboard')->name('member');
// Route::get('/member', function(){
//     return view('client.layout.member');
// })->name('member');

// Order Package
Route::get('/order/{package}', 'OrderController@create')->name('create.order');

Route::get('/profile', 'MemberController@showProfile')->name('profile');
Route::post('/updatedProfile', 'MemberController@storeProfile')->name('store.profile');

// login facebook and google routes
Route::get('auth/{provider}', 'Auth\AuthController@redirectToProvider');
Route::get('auth/{provider}/callback', 'Auth\AuthController@handleProviderCallback');

// fitur member
Route::group(['prefix' => 'member'], function () {
    Route::get('/dashboard', 'MemberController@dashboard')->name('member.dashboard');
    Route::post('/loadmore/load_post', 'MemberController@load_post')->name('loadmore.load_post');
    Route::get('/tickets', 'TicketController@index')->name('all.ticket');
    Route::get('/new-ticket', 'TicketController@create')->name('new.ticket');
    Route::get('/manage', 'MemberController@manage')->name('member.manage');
    // route transaction
    Route::get('/transactions', 'TransactionController@index')->name('transaction.index');
    Route::get('/transaction/create-order/{package}', 'TransactionController@createOrder')->name('transaction.create_order');
    Route::get('/transaction/checkout/{orderId}', 'TransactionController@checkout')->name('transaction.checkout');
    Route::get('/transaction/payment/finish', function () {
        return view('client.pages.member.transaction.notification-payment', ['header_title' => 'Transaksi Selesai', 'breadcrumb_item' => 'Transaksi Status']);
    })->name('transaction.finish');
    Route::post('/transaction/store', 'TransactionController@store')->name('transaction.store');
    Route::post('/transaction/notification/handler', 'TransactionController@notificationHandler')->name('notification.handler');

    Route::get('/affiliate', 'MemberController@affiliate')->name('member.affiliate');
    Route::get('/news', 'MemberController@news')->name('member.news');
    Route::get('/upgrade', 'MemberController@upgrade')->name('member.upgrade');
});

Route::group(['prefix' => 'admin'], function () {
    Voyager::routes();
});
