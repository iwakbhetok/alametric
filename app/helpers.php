<?php

function voyager_image($path, $modifier = 'default'){
    $img = '.'.pathinfo('storage/'.$path, PATHINFO_EXTENSION);
    $imgName = str_replace($img,'', $path);
    $img = $imgName.'-cropped'.$img;

//     $objPath = explode('/',$path);
//     $subDirectory = $objPath[1];
//     $objFile = explode('.',$objPath[2]);
//     $model = $objPath[0];
//     $imgName = $objFile[0];
//     $ext = pathinfo($path, PATHINFO_EXTENSION);
// if($modifier != 'default'){
//     $img = $model.'/'. $subDirectory .'/'.$imgName.'-'.$modifier.'.'.$ext;
// }
// else{
//     $img = $model.'/'.$imgName.'-'.$ext;
// }

return $img;
}