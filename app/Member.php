<?php

namespace App;

use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use App\Notifications\ResetPassword as ResetPasswordNotification;

class Member extends Authenticatable // implements MustVerifyEmail
{
    use Notifiable;

    protected $guard = 'member';

    protected $fillable = ['name', 'email', 'password', 'provider', 'provider_id'];

    // public function sendEmailVerificationNotification()
    // {
    //     // TODO: Implement sendEmailVerificationNotification() method.
    //     $this->notify(new \App\Notifications\VerifyEmail($this));
    // }

    public function sendPasswordResetNotification($token)
    {
        // Your your own implementation.
        $this->notify(new ResetPasswordNotification($token));
    }
}
