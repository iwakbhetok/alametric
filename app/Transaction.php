<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Transaction extends Model
{
    protected $fillable = [
        'order_id',
        'inv_number',
        'description',
        'amount',
        'status',
        'snap_token',
    ];

    public function setPending()
    {
        $this->attributes['status'] = 'PENDING';
        self::save();
    }

    public function setSuccess()
    {
        $this->attributes['status'] = 'SUCCESS';
        self::save();
    }

    public function setFailed()
    {
        $this->attributes['status'] = 'FAILED';
        self::save();
    }

    public function setExpired()
    {
        $this->attributes['status'] = 'EXPIRED';
        self::save();
    }
}
