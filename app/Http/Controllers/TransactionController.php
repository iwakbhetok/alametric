<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Veritrans_Config;
use Veritrans_Snap;
use Veritrans_Notification;
use App\Package;
use App\Order;
use App\Member;
use App\Transaction;
use Illuminate\Support\Facades\DB;

class TransactionController extends Controller
{
    protected $request;
    
    public function __construct(Request $request)
    {
        $this->request = $request;
 
        // Set midtrans configuration
        Veritrans_Config::$serverKey = config('services.midtrans.serverKey');
        Veritrans_Config::$isProduction = config('services.midtrans.isProduction');
        Veritrans_Config::$isSanitized = config('services.midtrans.isSanitized');
        Veritrans_Config::$is3ds = config('services.midtrans.is3ds');
    }

    public function index()
    {
        $header_title = 'Transaksi';
        $breadcrumb_item = 'Transaksi';
        $orders = DB::table('orders')
            ->join('packages', 'packages.id', '=', 'orders.package_id')
            ->select('orders.id as orderId','orders.status as order_status', 'packages.*')
            ->where('orders.member_id', Auth::guard('member')->user()->id)
            ->get();
        return view('client.pages.member.transactions', compact('header_title','breadcrumb_item','orders'));
    }

    public function createOrder($package)
    {
        $header_title = 'Transaksi';
        $breadcrumb_item = 'Transaksi';
        $package_detail = Package::where('slug', $package)->first();
        if(!empty($package_detail)){
            $member_id = Auth::guard('member')->user()->id;
            // check if member has order with same package
            $checkOrder = Order::where('member_id', $member_id)->where('package_id', $package_detail->id)->orWhere('status', 'PENDING')->get();
            
            if(count($checkOrder) < 1){
                // save order
                $order = new Order();
                $order->member_id = $member_id;
                $order->package_id = $package_detail->id;
                $order->status = 'PENDING';
                $order->save();
            }else{
                // reject order    
                $notification = array(
                    'message' => 'Maaf, anda masih memiliki order paket yang tertunda, mohon cek menu <a href="/member/transactions">Transaksi</a>.', 
                    'alert-type' => 'info'
                );
                return redirect('/member/upgrade')->with($notification);
            }
            
        }else{
            return view('client.pages.member.error', compact('header_title','breadcrumb_item'));
        }
    }

    public function checkout($orderId){
        $header_title = 'Transaksi';
        $breadcrumb_item = 'Transaksi';
        $order_detail = DB::table('orders')
            ->join('packages', 'packages.id', '=', 'orders.package_id')
            ->select('orders.id as orderId','orders.status as order_status', 'packages.*')
            ->where('orders.id', $orderId)
            ->first();
        return view('client.pages.member.transaction.checkout', compact('header_title','breadcrumb_item','order_detail'));
    }

    public function store(Request $request)
    {
        
        \DB::transaction(function(){
            $member = Member::where('id', Auth::guard('member')->user()->id)->get();
            $digits = 4; echo random_int( 10 ** ( $digits - 1 ), ( 10 ** $digits ) - 1); 
            // Save transaction ke database
            $transaction = Transaction::create([
                'order_id' => $this->request->order_id,
                'inv_number' => 'INV-'.date('m').'-ALA-'.date('Y').'-'.$digits,
                'description'   => 'Pemesanan paket '.$this->request->package_name.' berhasil dilakukan',
                'amount' => $this->request->amount,
                'status' => 'PENDING',
            ]);
            // return response()->json($transaction->order_id);
 
            // Buat transaksi ke midtrans kemudian save snap tokennya.
            $payload = [
                'transaction_details' => [
                    'order_id'      => $transaction->id,
                    'gross_amount'  => $transaction->amount,
                ],
                'customer_details' => [
                    'first_name'    => $member->name,
                    'email'   => $member->email,
                ],
                'item_details' => [
                    [
                        'id'       => $transaction->id,
                        'price'    => $transaction->amount,
                        'quantity' => 1,
                        'name'     => ucwords(str_replace('_', ' ', $transaction->description))
                    ]
                ]
            ];
            $snapToken = Veritrans_Snap::getSnapToken($payload);
            $transaction->snap_token = $snapToken;
            $transaction->save();
 
            // Beri response snap token
            $this->response['snap_token'] = $snapToken;
        });
 
        return response()->json($this->response);
    }

    public function notificationHandler(Request $request)
    {
        $notif = new Veritrans_Notification();
        \DB::transaction(function() use($notif) {
 
          $transaction = $notif->transaction_status;
          $type = $notif->payment_type;
          $orderId = $notif->order_id;
          $fraud = $notif->fraud_status;
          $trans = Transaction::findOrFail($orderId);
 
          if ($transaction == 'capture') {
 
            // For credit card transaction, we need to check whether transaction is challenge by FDS or not
            if ($type == 'credit_card') {
 
              if($fraud == 'challenge') {
                // TODO set payment status in merchant's database to 'Challenge by FDS'
                // TODO merchant should decide whether this transaction is authorized or not in MAP
                // $donation->addUpdate("Transaction order_id: " . $orderId ." is challenged by FDS");
                $trans->setPending();
              } else {
                // TODO set payment status in merchant's database to 'Success'
                // $donation->addUpdate("Transaction order_id: " . $orderId ." successfully captured using " . $type);
                $trans->setSuccess();
              }
 
            }
 
          } elseif ($transaction == 'settlement') {
 
            // TODO set payment status in merchant's database to 'Settlement'
            // $donation->addUpdate("Transaction order_id: " . $orderId ." successfully transfered using " . $type);
            $trans->setSuccess();
 
          } elseif($transaction == 'pending'){
 
            // TODO set payment status in merchant's database to 'Pending'
            // $donation->addUpdate("Waiting customer to finish transaction order_id: " . $orderId . " using " . $type);
            $trans->setPending();
 
          } elseif ($transaction == 'deny') {
 
            // TODO set payment status in merchant's database to 'Failed'
            // $donation->addUpdate("Payment using " . $type . " for transaction order_id: " . $orderId . " is Failed.");
            $trans->setFailed();
 
          } elseif ($transaction == 'expire') {
 
            // TODO set payment status in merchant's database to 'expire'
            // $donation->addUpdate("Payment using " . $type . " for transaction order_id: " . $orderId . " is expired.");
            $trans->setExpired();
 
          } elseif ($transaction == 'cancel') {
 
            // TODO set payment status in merchant's database to 'Failed'
            // $donation->addUpdate("Payment using " . $type . " for transaction order_id: " . $orderId . " is canceled.");
            $trans->setFailed();
 
          }
 
        });
 
        return;
    }


}
