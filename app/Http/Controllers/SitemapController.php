<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Sitemap;
use App\Post;

class SitemapController extends Controller
{
    public function index(){
		$data['post'] = Post::orderBy('updated_at', 'DESC')->get();
		
		return response()->view('client.pages.sitemap.index', $data)->header('Content-Type', 'text/xml');
	}
}
