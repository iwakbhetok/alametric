<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\SendsPasswordResetEmails;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Str;

class ForgotPasswordController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Password Reset Controller
    |--------------------------------------------------------------------------
    |
    | This controller is responsible for handling password reset emails and
    | includes a trait which assists in sending these notifications from
    | your application to your users. Feel free to explore this trait.
    |
    */

    use SendsPasswordResetEmails;

    public function showLinkRequestForm()
    {
        return view('client.pages.auth.forgot-password');
    }

    // public function sendResetLinkEmail(Request $request)
    // {
    //     $request->validate(['email' => 'required|email']);
    //     $member = DB::table('members')->where('email', '=', $request->email)
    //             ->first();

    //     //Check if the member exists
    //     if ($member == NULL) {
    //         return redirect()->back()->withErrors(['email' => trans('Maaf, email tidak terdaftar.')]);
    //     }

    //     //Create Password Reset Token
    //     DB::table('password_resets')->insert([
    //         'email' => $request->email,
    //         'token' => Str::random(60),
    //         'created_at' => \Carbon\Carbon::now()
    //     ]);

    //     //Get the token just created above
    //     $tokenData = DB::table('password_resets')
    //         ->where('email', $request->email)->first();

    //     if ($this->sendResetEmail($request->email, $tokenData->token)) {
    //         // $this->sendResetLinkEmail($request);
    //         return redirect()->back()->with('status', trans('A reset link has been sent to your email address.'));
    //     } else {
    //         return redirect()->back()->withErrors(['error' => trans('A Network Error occurred. Please try again.')]);
    //     }
    // }

    private function sendResetEmail($email, $token)
    {
    //Retrieve the user from the database
    $member = DB::table('members')->where('email', $email)->select('name', 'email')->first();
    if($member != NULL){
        return true;
    }else{
        return false;
    }
    //Generate, the password reset link. The token generated is embedded in the link
    // $link = config('base_url') . 'password/reset/' . $token . '?email=' . urlencode($user->email);

        // try {
        // //Here send the link with CURL with an external email API 
        //     return true;
        // } catch (\Exception $e) {
        //     return false;
        // }
    }
}
