<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use App\Providers\RouteServiceProvider;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Http\Request;
use App\User;
use Auth;

class LoginController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles authenticating users for the application and
    | redirecting them to your home screen. The controller uses a trait
    | to conveniently provide its functionality to your applications.
    |
    */

    use AuthenticatesUsers;

    /**
     * Where to redirect users after login.
     *
     * @var string
     */
    protected $redirectTo = '/';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        // $this->middleware('guest')->except('logout');
        $this->middleware('guest:member')->except('logout');
    }

    public function getLogin()
    {
        return view('client.pages.auth.login');
    }

    public function getReLogin()
    {
        return view('client.pages.auth.login');
    }

    public function postLogin(Request $request)
    {
        // Validate the form data
        $this->validate($request, [
        'email' => 'required|email',
        'password' => 'required'
        ]);

        // Attempt to log the user in
        // Passwordnya pake bcrypt
        if (Auth::guard('member')->attempt(['email' => $request->email, 'password' => $request->password])) 
        {
            session()->flash('status', "Welcome, " . Auth::guard('member')->user()->name);
            return redirect('/member/dashboard');
        }else{
            return redirect('/login');
        }

    }

    public function logout(Request $request)
    {
        Auth::guard('member')->logout();
        $request->session()->forget('order_package');
        $request->session()->flush();
        return redirect('/login');
    }

}
