<?php

namespace App\Http\Controllers\Auth;

use App\Member;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Laravel\Socialite\Facades\Socialite;

class AuthController extends Controller
{

    public function redirectToProvider($provider)
    {
        return Socialite::driver($provider)->redirect();
    }

    /**
     * Obtain the user information from provider.  Check if the user already exists in our
     * database by looking up their provider_id in the database.
     * If the user exists, log them in. Otherwise, create a new user then log them in. After that
     * redirect them to the authenticated users homepage.
     *
     * @return Response
     */
    public function handleProviderCallback($provider)
    {
        $member = Socialite::driver($provider)->stateless()->user();
        $authMember = $this->findOrCreateMember($member, $provider);
        Auth::guard('member')->login($authMember);
        return redirect('/member/dashboard');
    }

    /**
     * If a user has registered before using social auth, return the user
     * else, create a new user object.
     * @param  $user Socialite user object
     * @param $provider Social auth provider
     * @return  Member
     */
    public function findOrCreateMember($member, $provider)
    {
        $authMember = Member::where('provider_id', $member->id)->first();
        if ($authMember) {
            return $authMember;
        }
        else{
            $data = Member::create([
                'name'     => $member->name,
                'email'    => !empty($member->email)? $member->email : '' ,
                'provider' => $provider,
                'provider_id' => $member->id
            ]);
            return $data;
        }
    }
}