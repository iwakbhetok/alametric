<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Member;
use Illuminate\Support\Facades\Auth;
use TCG\Voyager\Models\Post;
use App\Order;
use App\Package;
use Illuminate\Support\Str;

class MemberController extends Controller
{
    public function dashboard()
    {
        $header_title = 'Dashboard';
        $breadcrumb_item = 'Dashboard';
        $posts = Post::where(['status' => 'published'])->orderBy('created_at', 'desc')->limit(4)->get();
        // $notification = session('');
        $session_order = session('order_package');
        if ($session_order) {
            // save to database table order
            $package = Package::where('slug', $session_order)->first();
            if (!empty($package)) {
                $member_id = Auth::guard('member')->user()->id;
                $order = new Order();
                $order->member_id = $member_id;
                $order->package_id = $package->id;
                $order->status = 'PENDING';
                $order->save();
                $order_package = $session_order;
                $invalid_package = false;
            } else {
                $invalid_package = true;
                $order_package = null;
            }
        } else {
            // just login
            $invalid_package = null;
            $order_package = null;
        }

        $memberOrders = Order::where('member_id', Auth::guard('member')->user()->id)->where('status', 'PENDING')->distinct()->count('package_id');
        return view('client.pages.member.dashboard', compact('header_title', 'breadcrumb_item', 'posts', 'order_package', 'invalid_package', 'memberOrders'));
    }

    public function load_post(Request $request)
    {
        if ($request->ajax()) {
            if ($request->id > 0) {
                $data = Post::where('id', '<', $request->id)
            ->where('status', 'PUBLISHED')
            ->orderBy('created_at', 'DESC')
            ->limit(4)
            ->get();
            } else {
                $data = Post::orderBy('created_at', 'DESC')
            ->where('status', 'PUBLISHED')
            ->limit(4)
            ->get();
            }
            $output = '';
            $last_id = '';

            if (!$data->isEmpty()) {
                $output .='<div class="row">';
                foreach ($data as $row) {
                    $output .= '
            <div class="col-md-3">
                <div class="card">
                    <div class="card-img" style="background-image:url('.asset('storage/'.$row->image).');background-size:cover;background-repeate:no-repeat;max-height:150px;height:150px;"></div>
                    <div class="card-body">
                        <h4 class="m-t-10">'.substr(strip_tags($row->title), 0, 25) .'</h4>
                        <p class="m-b-20">'.substr(strip_tags($row->excerpt), 0, 35) .'</p>
                        <div class="d-flex align-items-center justify-content-between">
                            <p class="m-b-0 text-dark font-weight-semibold font-size-15">'.\Carbon\Carbon::parse($row->created_at)->format('M d, Y').'</p>
                            <a class="text-primary btn btn-sm btn-hover" href="'.route('blog.detail', $row->slug).'" target="_blank">
                                <span>Read More</span>
                            </a>
                        </div>
                    </div>
                    <div class="card-footer">
                        <div class="d-flex align-items-center m-t-5">
                            <div class="avatar avatar-image avatar-sm">
                                <img src="'.asset('member_assets/img/avatar-icon.png').'" alt="">
                            </div>
                            <div class="m-l-10">
                                <span class="font-weight-semibold">Admin</span>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            ';
                    $last_id = $row->id;
                }
                $output .= '</div>';
                $output .= '
        <div class="m-t-10 pagination justify-content-center" id="load_more">
        <button id="load-more" data-id="'.$last_id.'" class="btn btn-primary m-r-5">
            <i class="anticon anticon-loading m-r-5"></i>
            <span>Load More</span>
        </button>
        </div>
        ';
            } else {
                $output .= '
        <div class="m-t-10 pagination justify-content-center" id="load_more">
        <button id="load-more" class="btn btn-info m-r-5">
            <i class="anticon anticon-loading m-r-5"></i>
            <span>No Data Found</span>
        </button>
        </div>
        ';
            }
            echo $output;
        }
    }

    public function showProfile()
    {
        $email = Auth::guard('member')->user()->email;
        $member = Member::where('email', $email)->first();
        return view('client.pages.member.profile', compact('member'));
    }

    public function storeProfile(Request $request)
    {
        $this->validate($request, [
            'name' => ['required'],
            'email' => ['required'],
            ]);
        $member = Member::find(Auth::guard('member')->user()->id);
        $member->name = $request->name;
        $member->email = $request->email;
        $member->address = $request->address;
        $member->save();
        session()->flash('status', "Profile Updated Successfully");
        return redirect()->back();
    }

    public function manage()
    {
        $header_title = 'Manajemen Paket';
        $breadcrumb_item = 'Manajemen Paket';
        return view('client.pages.member.manage-packages', compact('header_title', 'breadcrumb_item'));
    }

    public function affiliate()
    {
        $header_title = 'Program Affiliate';
        $breadcrumb_item = 'Program Affiliate';
        return view('client.pages.member.affiliate-program', compact('header_title', 'breadcrumb_item'));
    }

    public function news()
    {
        $header_title = 'Berita Pilihan';
        $breadcrumb_item = 'Berita Pilihan';
        return view('client.pages.member.news', compact('header_title', 'breadcrumb_item'));
    }

    public function upgrade()
    {
        $header_title = 'Upgrade Paket';
        $breadcrumb_item = 'Upgrade Paket';
        $packages = Package::where('status', 'ACTIVE')->get();
        return view('client.pages.member.upgrade-package', compact('header_title', 'breadcrumb_item', 'packages'));
    }
}
