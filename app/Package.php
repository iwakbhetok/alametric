<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Package extends Model
{
    public function package_categories()
    {
        return $this->morphMany('App\PackageCategory', 'package_category');
    }
}
